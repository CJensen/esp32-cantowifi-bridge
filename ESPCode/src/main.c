/**
 * @file main.c
 * @author Clemens Jensen (clemens_jensen@yahoo.com)
 * @brief Entry point for program code. 
 * @version 0.1
 * @date 2022-05-17
 * 
 * @copyright Copyright (c) 2022
 * 
 */
#include "tasks.h"
#include "wifi.h"
#include "gps.h"

/**
 * @brief main function starting different task that run in "parallel" using RTOS
 * 
 * 
 */
void app_main()
{
    #ifdef BUILD_FLAG_TRANS
    wifi_init_softap();
    gps_parser_config_t config = GPS_PARSER_CONFIG_DEFAULT();    
    gps_init(&config);
    #else
    wifi_init_sta();
    #endif
    
    init_can();

    xTaskCreatePinnedToCore(task_CAN_to_UDP, "CAN_to_UDP", 6 * 4096, NULL, 2, NULL, tskNO_AFFINITY);
    xTaskCreatePinnedToCore(task_UDP_to_CAN, "UDP_to_CAN", 2 * 4096, NULL, 2, NULL, tskNO_AFFINITY);

}