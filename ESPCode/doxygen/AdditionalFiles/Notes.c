/*
Black   = Gnd
White   = TX
Green   = RX
Red     = Vcc

RX ESP = Pin 26
TX ESP = Pin 25
*/

/*
$GNRMC,092547.00,V,,,,,,,130521,,,N*6A      =      (xxRMC,              // RMCMessageID(xx=currentTalkerID)
                                                    time,               // UTCtime,see note on UTC representation
                                                    status,             // Status V:Navigation receiver warning A:Datavalid
                                                    lat,                // Latitude(degrees&minutes),seeformatdescription
                                                    NS,                 // North/South indicator
                                                    long,               // Longitude(degrees&minutes),seeformatdescription
                                                    EW,                 // East/West indicator
                                                    spd,                // Speed over ground
                                                    cog,                // Cours eover ground
                                                    date,               // Date in day,month,year format
                                                    mv,                 // Magnetic variation value (blank-notsupported)
                                                    mvEW,               // MagneticvariationE/Windicator (blank-notsupported)
                                                    posMode,            // ModeIndicator,see position fix flags
                                                    navStatus,          // Navigational status indicator (V=Equipment is not providing navigational statusin formation)
                                                    cs                  // Checksum
                                                    )                   //

$GNVTG,,,,,,,,,N*2E                         =       (xxVTG,             // VTGMessageID(xx=currentTalkerID)
                                                    cogt,               // Courseoverground(true)
                                                    T,                  // Fixedfield:true
                                                    cogm,               // Courseoverground(magnetic),notoutput
                                                    M,                  // Fixedfield:magnetic
                                                    knots,              // Speed over ground
                                                    N,                  // Fixed field: knots
                                                    kph,                // Speed over ground
                                                    K,                  // Fixedfield:kilometers per hour
                                                    posMode,            //
                                                    cs                  //
                                                    )

$GNGGA,092547.00,,,,,0,00,99.99,,,,,,*75    =       (xxGGA,             //
                                                    time,               //
                                                    lat,                //
                                                    NS,                 //
                                                    long,               //
                                                    EW,                 //
                                                    quality,            // 0: NoFix/Invalid 1: Standard GPS(2D/3D) 2: Differential GPS 6:Estimated(DR) Fix
                                                    numSV,              // Number of satellites used
                                                    HDOP,               // Horizontal Dilution of Precision
                                                    alt,                // Altitude above mean sea level
                                                    uAlt,               // Altitude units: meters(fixedfield)
                                                    sep,                // Geoid separation: difference between geoid and mean sea level
                                                    uSep,               // Separation units: meters(fixedfield)
                                                    diffAge,            // Age of differential corrections(blank when DGPS is not used)
                                                    diffStation,        // ID of station providing differential corrections (blankwhenDGPSisnotused)
                                                    cs                  //
                                                    )

$GNGSA,A,1,,,,,,,,,,,,,99.99,99.99,99.99*2E =       (xxGSA,
                                                    opMode,
                                                    navMode,
                                                    sv,                 //(repeated block) Sattellite Number
                                                    ,
                                                    PDOP,
                                                    HDOP,
                                                    VDOP,
                                                    systemID,
                                                    cs,
                                                    )

$GPGSV,1,1,01,02,,,26*7E                    =       (xxGSV,             
                                                    numMsg,             // Numberofmessages,totalnumberofGSVmessagesbeingoutput
                                                    msgNum,             // Numberofthismessage
                                                    numSV,              // Numberofsatellitesinview
                                                    REPEATED BLOCK
                                                    SV,                 // SatelliteID
                                                    elv,                // Elevation(range0-90)
                                                    az,                 // Azimuth,(range0-359)
                                                    cno                 // Signalstrength(C/N0,range0-99),blankwhennottracking
                                                    END OF REPEATED BLOCK
                                                    singalID,           //
                                                    cs                  //
                                                    )

$GNGLL,,,,,092547.00,V,N*59                 =       (GNGLL,
                                                    lat,                // Latitude(degrees&minutes)
                                                    NS,                 // North/Southindicator
                                                    long,               // Longitude(degrees&minutes)
                                                    EW,                 // East/Westindicator
                                                    time,               // UTCtime
                                                    status,             // V=Datainvalidorreceiverwarning,A=Datavalid
                                                    posMode,            // Positioningmode
                                                    cs
                                                    )                 // Checksum
*/

/* Without Fix
11:25:45.550 >
11:25:46.297 > I (12000) uart_events: uart[2] event:
11:25:46.298 > I (12000) uart_events: [UART PATTERN DETECTED] pos: 39, buffered size: 40
11:25:46.309 > I (12000) uart_events: read data: $GNRMC,092547.00,V,,,,,,,130521,,,N*6A
11:25:46.314 > I (12010) uart_events: read pat : 
11:25:46.320 >
11:25:46.320 > I (12020) uart_events: uart[2] event:
11:25:46.326 > I (12020) uart_events: [UART PATTERN DETECTED] pos: 20, buffered size: 21
11:25:46.332 > I (12030) uart_events: read data: $GNVTG,,,,,,,,,N*2E
11:25:46.337 > I (12030) uart_events: read pat : 
11:25:46.339 >
11:25:46.363 > I (12070) uart_events: uart[2] event:
11:25:46.364 > I (12070) uart_events: [UART PATTERN DETECTED] pos: 41, buffered size: 42
11:25:46.375 > I (12070) uart_events: read data: $GNGGA,092547.00,,,,,0,00,99.99,,,,,,*75
11:25:46.380 > I (12080) uart_events: read pat : 
11:25:46.383 >
11:25:46.410 > I (12110) uart_events: uart[2] event:
11:25:46.411 > I (12110) uart_events: [UART PATTERN DETECTED] pos: 44, buffered size: 45
11:25:46.422 > I (12110) uart_events: read data: $GNGSA,A,1,,,,,,,,,,,,,99.99,99.99,99.99*2E
11:25:46.426 > I (12120) uart_events: read pat : 
11:25:46.430 >
11:25:46.457 > I (12160) uart_events: uart[2] event:
11:25:46.457 > I (12160) uart_events: [UART PATTERN DETECTED] pos: 44, buffered size: 45
11:25:46.468 > I (12160) uart_events: read data: $GNGSA,A,1,,,,,,,,,,,,,99.99,99.99,99.99*2E
11:25:46.474 > I (12170) uart_events: read pat : 
11:25:46.477 >
11:25:46.484 > I (12190) uart_events: uart[2] event:
11:25:46.485 > I (12190) uart_events: [UART PATTERN DETECTED] pos: 25, buffered size: 26
11:25:46.495 > I (12190) uart_events: read data: $GPGSV,1,1,01,02,,,26*7E
11:25:46.500 > I (12190) uart_events: read pat : 
11:25:46.500 >
11:25:46.506 > I (12210) uart_events: uart[2] event:
11:25:46.506 > I (12210) uart_events: [UART PATTERN DETECTED] pos: 17, buffered size: 18
11:25:46.518 > I (12210) uart_events: read data: $GLGSV,1,1,00*65
11:25:46.525 > I (12220) uart_events: read pat : 
11:25:46.525 >
11:25:46.533 > I (12240) uart_events: uart[2] event:
11:25:46.533 > I (12240) uart_events: [UART PATTERN DETECTED] pos: 28, buffered size: 29
11:25:46.544 > I (12240) uart_events: read data: $GNGLL,,,,,092547.00,V,N*59 
11:25:46.549 > I (12240) uart_events: read pat : 
11:25:46.549 >
*/

/* With FIX
13:48:13.807 > I (9410) uart_events: uart[2] event:
13:48:13.807 > I (9410) uart_events: [UART PATTERN DETECTED] pos: 67, buffered size: 68
13:48:13.818 > I (9420) uart_events: read data: $GNRMC,114814.00,A,5420.56193,N,01008.32902,E,0.419,,130521,,,A*67
13:48:13.825 > I (9430) uart_events: read pat : 
13:48:13.830 >
13:48:13.844 > I (9450) uart_events: uart[2] event:
13:48:13.844 > I (9450) uart_events: [UART PATTERN DETECTED] pos: 34, buffered size: 35
13:48:13.855 > I (9450) uart_events: read data: $GNVTG,,T,,M,0.419,N,0.777,K,A*36
13:48:13.860 > I (9460) uart_events: read pat : 
13:48:13.863 > 
13:48:13.922 > I (9530) uart_events: uart[2] event:
13:48:13.922 > I (9530) uart_events: [UART PATTERN DETECTED] pos: 73, buffered size: 74
13:48:13.932 > I (9530) uart_events: read data: $GNGGA,114814.00,5420.56193,N,01008.32902,E,1,09,0.91,38.4,M,44.3,M,,*7E
13:48:13.938 > I (9540) uart_events: read pat : 
13:48:13.943 >
13:48:13.979 > I (9590) uart_events: uart[2] event:
13:48:13.979 > I (9590) uart_events: [UART PATTERN DETECTED] pos: 55, buffered size: 56
13:48:13.991 > I (9590) uart_events: read data: $GNGSA,A,3,31,18,29,26,25,16,05,,,,,,2.07,0.91,1.86*1F
13:48:13.997 > I (9600) uart_events: read pat : 
13:48:14.001 >
13:48:14.027 > I (9630) uart_events: uart[2] event:
13:48:14.027 > I (9630) uart_events: [UART PATTERN DETECTED] pos: 45, buffered size: 46
13:48:14.039 > I (9630) uart_events: read data: $GNGSA,A,3,84,76,,,,,,,,,,,2.07,0.91,1.86*13
13:48:14.045 > I (9640) uart_events: read pat : 
13:48:14.047 >
13:48:14.096 > I (9700) uart_events: uart[2] event:
13:48:14.096 > I (9700) uart_events: [UART PATTERN DETECTED] pos: 65, buffered size: 66
13:48:14.107 > I (9700) uart_events: read data: $GPGSV,3,1,10,04,08,307,,05,24,071,22,09,07,339,,16,28,298,29*75
13:48:14.113 > I (9710) uart_events: read pat : 
13:48:14.118 >
13:48:14.170 > I (9780) uart_events: uart[2] event:
13:48:14.170 > I (9780) uart_events: [UART PATTERN DETECTED] pos: 69, buffered size: 70
13:48:14.179 > I (9780) uart_events: read data: $GPGSV,3,2,10,18,49,174,20,20,26,107,18,25,23,131,18,26,54,289,24*78
13:48:14.185 > I (9790) uart_events: read pat : 
13:48:14.191 >
13:48:14.215 > I (9820) uart_events: uart[2] event:
13:48:14.215 > I (9820) uart_events: [UART PATTERN DETECTED] pos: 43, buffered size: 44
13:48:14.225 > I (9820) uart_events: read data: $GPGSV,3,3,10,29,60,073,22,31,39,224,12*7E
13:48:14.231 > I (9830) uart_events: read pat : 
13:48:14.234 >
13:48:14.281 > I (9890) uart_events: uart[2] event:
13:48:14.282 > I (9890) uart_events: [UART PATTERN DETECTED] pos: 63, buffered size: 64
13:48:14.293 > I (9890) uart_events: read data: $GLGSV,3,1,09,67,12,350,15,68,16,040,,69,01,085,,74,34,164,*67
13:48:14.299 > I (9900) uart_events: read pat : 
13:48:14.302 >
13:48:14.353 > I (9960) uart_events: uart[2] event:
13:48:14.353 > I (9960) uart_events: [UART PATTERN DETECTED] pos: 67, buffered size: 68
13:48:14.363 > I (9960) uart_events: read data: $GLGSV,3,2,09,75,74,258,,76,27,322,33,84,53,044,29,85,62,231,19*64
13:48:14.368 > I (9970) uart_events: read pat : 
13:48:14.374 >
13:48:14.382 > I (9990) uart_events: uart[2] event:
13:48:14.383 > I (9990) uart_events: [UART PATTERN DETECTED] pos: 28, buffered size: 29
13:48:14.394 > I (9990) uart_events: read data: $GLGSV,3,3,09,86,12,228,*59
13:48:14.400 > I (10000) uart_events: read pat : 
13:48:14.400 >
13:48:14.437 > I (10040) uart_events: uart[2] event:
13:48:14.437 > I (10040) uart_events: [UART PATTERN DETECTED] pos: 51, buffered size: 52
13:48:14.447 > I (10040) uart_events: read data: $GNGLL,5420.56193,N,01008.32902,E,114814.00,A,A*76
13:48:14.453 > I (10050) uart_events: read pat : 
13:48:14.457 >
*/

/*
22:05:27.103 > Lat: 54.342751
22:05:27.103 > Long: 10.138803
22:05:27.103 > Lat: 20640bc0            543427520/10000000
22:05:27.103 > Long: 60b0f00            101388032/10000000
22:05:27.107 > Speed: 0.355965 [m/s]
*/