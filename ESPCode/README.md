
# Mainpage
## Parts used

1. ESP32-DevKitC-VIE
2. SN65HVD23X CAN Tranceiver
3. WiFi Antenna with I-PEX MHF2 Connector
4. Micro-USP Connector
5. GPS Receiver

## IDE
   
Project developed with **Visual Studio Code** an **Platformio** extension to compile and upload code to ESP32.
   
### Setup

1. Download Visual Studio Code
2. Install PlatformIO extension
3. Restart Visual Studio
4. Wait until Platformio is set up (may take several minutes)
5. Open Workspace from Repository
6. Wait again for PlatformIO to finish rebuilding IntelliSense index
   
#### Build and Upload
   
To build and upload the code check the blue bar below the editor.
   
#### Menuconfig
   
To configure some properties of the ESP the so called menuconfig is used. It can be started by clicking on the Platformio logo on the left. Menuconfig can then be found under Platform. Clicking it should start a terminal. You can navigate with *J*/*K* for up and down and *Enter*/*Return* to go into menu or back out of it.

## Telemetry

Each CAN message gets converted to a UDP Frame and packaged into a larger package. On the receiver the package gets converted to CAN messages again which in turn can be received (eg. by a Vector VN1610) for displaying them.

## GPS
   
The Transmitter installed in the car has a GPS Receiver that communicates to the ESP by UART. It uses the NMEA standart which is then parsed and converted to can messages on the esp.

## Laptime

GPS based laptime based on distance to start.
Start can be set by CAN Command, and is logged to CAN bus. After a lap is detected a lap counter is incremented and the laptime
is sent to CAN.
## Links

- ESP Documentation: [ESP-IDF Programming Guide v4.1.2 documentation](https://docs.espressif.com/projects/esp-idf/en/v4.1.2/get-started/index.html)

- VS Code: https://code.visualstudio.com/

## Future development:

- Add simple webserver that can be connected to by laptop to see state of the Radio without needing to connect to it.
- Add support for SPI CAN Tranceiver to receive/transmit on multiple CAN busses.