/**
 * @dir lib/Wifi
 * 
 * @brief Source and header files for the setup and usage of wifi.
 * 
 * # Overview
 * 
 * 
 * 
 * @file wifi.h
 * @author Clemens Jensen (clemens_jensen@yahoo.com)
 * @brief
 * @version 0.1
 * @date 2022-05-17
 * 
 * @copyright Copyright (c) 2022
 * 
 */
#ifndef WIFI_H
#define WIFI_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/param.h>

#include "esp_system.h"
#include "esp_wifi.h"
#include "esp_event.h"

#include "nvs_flash.h"
#include "esp_log.h"

#include <string.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/event_groups.h"
#include "esp_system.h"
#include "esp_wifi.h"
#include "esp_event.h"
#include "esp_log.h"
#include "nvs_flash.h"

#include "lwip/err.h"
#include "lwip/sys.h"

/**
 * @brief Defines WiFi AccessPoint information
 *
 * Run menuconfig to change these values
 *
 */
/// SSID name of wifi network = racecarA19
#define ESP_WIFI_SSID CONFIG_ESP_WIFI_SSID
/// password of network = 12345678a
#define ESP_WIFI_PASS CONFIG_ESP_WIFI_PASSWORD
/// Wifi channel = 1
#define ESP_WIFI_CHANNEL CONFIG_ESP_WIFI_CHANNEL
/// Maximum numbers of connections to the access point = 1
#define MAX_STA_CONN CONFIG_ESP_MAX_STA_CONN

/**
 * @brief Event handler for WiFi events like stations connecting/disconnectin
 *
 * @param arg
 * @param event_base
 * @param event_id
 * @param event_data
 */
void wifi_event_handler(void *arg, esp_event_base_t event_base,
                        int32_t event_id, void *event_data);

/**
 * @brief Initialize wifi as access point, eg. Transmitter
 *
 */
void wifi_init_softap(void);

/**
 * @brief Initialize wifi as station, eg. Receiver
 * 
 */
void wifi_init_sta(void);

#endif /* WIFI_H */