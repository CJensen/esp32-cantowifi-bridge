/**
 * @file laptime.c
 * @author Clemens Jensen (clemens_jensen@yahoo.com)
 * @brief
 * @version 0.1
 * @date 2022-07-12
 *
 * @copyright Copyright (c) 2022
 *
 */
#include "laptime.h"

laptime_t laptime_struct = {
	.previous_distance_to_start = 100,
	.laptime_timer_info_msg.identifier = 0x22C,
	.laptime_timer_info_msg.data_length_code = 4,
	.laptime_start_msg.identifier = 0x22E,
	.laptime_start_msg.data_length_code = 8,
	.laptime_status_msg.identifier = 0x22F,
	.start_zone = 3,
	.lap_counter = 1,
};

/**
 * @brief Initializes ESP Timer
 *
 * Initilizes Timer and sets the initial value to zero.
 *
 * @param group Timer group number (see ESP documentation)
 * @param timer Timer number (see ESP documentation)
 */
void init_laptimer(int group, int timer)
{
	timer_config_t timer_config = {
		.divider = TIMER_DIVIDER,
		.counter_dir = TIMER_COUNT_UP,
		.counter_en = TIMER_PAUSE,
		.alarm_en = TIMER_ALARM_EN,
		.auto_reload = false,
	}; // default clock source is APB

	timer_init(group, timer, &timer_config);

	timer_set_counter_value(group, timer, 0);
}

/**
 * @brief Convert CAN message to position used in all calculations
 *
 * @param position_message CAN message to be converted
 * @param laptime_struct Pointer to laptime struct
 */
void convert_message(twai_message_t position_message, laptime_t *laptime_struct)
{
	uint32_t GPS_lat = BYTES_TO_U32(position_message.data[0],
									position_message.data[1],
									position_message.data[2],
									position_message.data[3]);
	uint32_t GPS_long = BYTES_TO_U32(position_message.data[4],
									 position_message.data[5],
									 position_message.data[6],
									 position_message.data[7]);
	laptime_struct->gps_current_latitude = (float)GPS_lat / 10000000;
	laptime_struct->gps_current_longitude = (float)GPS_long / 10000000;
	laptime_struct->position_id++;
	// printf("%i\n",laptime_struct->position_id);
}

/**
 * @brief Calculates the haversine function for getting the distance on a sphere between two points.
 *
 * @param start_lat Latitude of the starting point.
 * @param start_lon Longitude of the starting point.
 * @param lat Latitude of the point the distance needs to be calculated to.
 * @param lon Longitude of the point the distance needs to be calculated to.
 * @return float Distance between the points.
 */
float dist(float start_lat, float start_lon, float lat, float lon)
{
	float dx, dy, dz;
	start_lon -= lon;
	start_lon *= TO_RAD, start_lat *= TO_RAD, lat *= TO_RAD;

	dz = sin(start_lat) - sin(lat);
	dx = cos(start_lon) * cos(start_lat) - cos(lat);
	dy = sin(start_lon) * cos(start_lat);
	return asin(sqrt(dx * dx + dy * dy + dz * dz) / 2) * 2 * R;
}

/**
 * @brief Checks if the current position is the start of a new lap.
 *
 * @param laptime_struct Pointer to laptime struct
 * @return true if a start is detected
 * @return false else
 */
bool check_for_start(laptime_t *laptime_struct)
{
	switch (laptime_struct->laptime_mode)
	{
	case AUTOCROSS:
		/* distance to autocross start */
		laptime_struct->distance_to_start = dist(laptime_struct->lap_info.autocross_start_latitude,
												 laptime_struct->lap_info.autocross_start_longitude,
												 laptime_struct->gps_current_latitude,
												 laptime_struct->gps_current_longitude);
		break;

	case ACCELERATION:
		/* distance to accel start */
		laptime_struct->distance_to_start = dist(laptime_struct->lap_info.accel_start_latitude,
												 laptime_struct->lap_info.accel_start_longitude,
												 laptime_struct->gps_current_latitude,
												 laptime_struct->gps_current_longitude);
		break;

	case SKIDPAD:
		/* distance to skidpad start */
		laptime_struct->distance_to_start = dist(laptime_struct->lap_info.skidpad_start_latitude,
												 laptime_struct->lap_info.skidpad_start_longitude,
												 laptime_struct->gps_current_latitude,
												 laptime_struct->gps_current_longitude);
		break;

	default:
		break;
	}

	if (laptime_struct->distance_to_start < laptime_struct->start_zone)
	{
		if (laptime_struct->previous_distance_to_start < laptime_struct->distance_to_start && laptime_struct->position_id > 20)
		{

			switch (laptime_struct->laptime_mode)
			{
			case AUTOCROSS:
				// code
				laptime_struct->lap_info.autocross_laptime = get_laptime_value(laptime_struct);
				laptime_struct->current_laptime = laptime_struct->lap_info.autocross_laptime;
				send_laptime_info(laptime_struct);
				laptime_struct->lap_info.autocross_laps++;
				break;

			case SKIDPAD:
				// code
				laptime_struct->lap_info.skidpad_laptime = get_laptime_value(laptime_struct);
				laptime_struct->current_laptime = laptime_struct->lap_info.skidpad_laptime;
				
				if (laptime_struct->lap_info.skidpad_laps % 5 == 0)
				{
					laptime_struct->lap_info.skidpad_laps = 1;
					laptime_struct->lap_info.skidpad_runs++;
					printf("New run started!\n");
					break;
				}
				send_laptime_info(laptime_struct);
				laptime_struct->lap_info.skidpad_laps++;
				break;

			case ACCELERATION:
				// code
				laptime_struct->lap_info.accel_laptime = get_laptime_value(laptime_struct);
				laptime_struct->current_laptime = laptime_struct->lap_info.accel_laptime;
				break;
			default:
				break;
			}
			laptime_struct->position_id = 0;
			return true;
		}
		laptime_struct->previous_distance_to_start = laptime_struct->distance_to_start;
	}
	return false;
}

/**
 * @brief Starts laptime and sends the starting position
 *
 * @param laptime_struct Pointer to laptime struct
 *
 */
void start_laptime(laptime_t *laptime_struct)
{
	laptime_struct->gps_latitude_start = laptime_struct->gps_current_latitude;
	laptime_struct->gps_longitude_start = laptime_struct->gps_current_longitude;
	laptime_struct->position_id = 0;
	laptime_struct->laptime_started = true; // TODO: change name
	printf("Lap started!\n");				// TODO: Remove
	timer_set_counter_value(TIMER_GROUP_0, TIMER_0, 0);
	timer_start(TIMER_GROUP_0, TIMER_0);

	send_start_position(laptime_struct);
}

/**
 * @brief Stops the laptime and resets the lap counter to 1
 *
 * @param laptime_struct Pointer to laptime struct
 */
bool stop_laptime(laptime_t *laptime_struct)
{
	timer_pause(TIMER_GROUP_0, TIMER_0);
	timer_set_counter_value(TIMER_GROUP_0, TIMER_0, 0);
	laptime_struct->lap_counter = 1;
	laptime_struct->laptime_started = false;
	printf("Laptime stopped!\n"); // TODO: Remove
	return true;
}

/**
 * @brief Get the current timer value
 * 
 * @param laptime_struct Pointer to laptime struct
 * @return uint32_t Current laptime value in milliseconds
 */
uint32_t get_laptime_value(laptime_t *laptime_struct)
{
	double laptime;

	timer_get_counter_time_sec(TIMER_GROUP_0, TIMER_0, &laptime);
	laptime_struct->current_laptime = (uint32_t)(laptime * 1000); // seconds to ms
	timer_set_counter_value(TIMER_GROUP_0, TIMER_0, 0);

	return laptime_struct->current_laptime;
}

/**
 * @brief Determines the action to take when Command is received.
 *
 *
 * @param can_message CAN message containing command for Laptime
 * @param laptime_struct Pointer to laptime struct
 */
void handle_commands(twai_message_t can_message, laptime_t *laptime_struct)
{
	switch (can_message.identifier)
	{
	// For using Laptime with other GPS data source over CAN
	case 0x22A:
		convert_message(can_message, laptime_struct);
		if (laptime_struct->laptime_started)
		{
			check_for_start(laptime_struct);
		}
		break;

	case 0x22D:

		switch (can_message.data[0]) // Mode Selection
		{
		case 0:
			printf("\n");
			break;
		case 1:
			laptime_struct->laptime_mode = AUTOCROSS;
			printf("\n");
			printf("Mode: AUTOCROSS\n");
			break;
		case 2: // TODO: implement later
			laptime_struct->laptime_mode = SKIDPAD;
			printf("\n");
			printf("Mode: SKIDPAD\n");
			break;
		case 3:
			laptime_struct->laptime_mode = ACCELERATION;
			printf("\n");
			printf("Mode: ACCELERATION\n");
			break;
		default:
			laptime_struct->laptime_mode = NO_MODE_SELECTED;
			break;
		}

		switch (can_message.data[1]) // Set Start
		{
		case 0:
			/* reset start */
			break;
		case 1:
			/* set start */
			set_start(laptime_struct);
		default:
			break;
		}

		switch (can_message.data[2]) // Start/Stop Command
		{
		case 0:
			/*do nothing*/
			break;
		case 1:
			stop_laptime(laptime_struct);
			break;
		case 2:
			start_laptime(laptime_struct);
		default:
			break;
		}

		switch (can_message.data[3])
		{
		case 0:
			/* code */
			break;
		case 1:
			/* code */
			reset_lap_counter(laptime_struct);
			break;
		default:
			break;
		}

		switch (can_message.data[4])
		{
		case 0:
			/* code */
			break;
		default:
			printf("Setting start zone to: %i\n", can_message.data[4]);
			laptime_struct->start_zone = can_message.data[4];
			break;
		}
		switch (can_message.data[5])
		{
		case 0:
			/* do nothing */
			break;
		case 1:
			poll_status(laptime_struct);
		default:
			break;
		}
		break;
	default:
		break;
	}
}

/**
 * @brief Copies the GPS position into laptime struct and determines current distance to start
 *
 * Currently unused due to issues with A22 not working with current Transmitter version
 * 
 * @param latitude Current latitude
 * @param longitude Current longitude
 * @param laptime_struct Pointer to laptime struct
 */
void uart_laptime(float latitude, float longitude, laptime_t *laptime_struct)
{
	laptime_struct->gps_current_latitude = latitude;
	laptime_struct->gps_current_longitude = longitude;

	if (laptime_struct->start_is_set)
	{
		laptime_struct->distance_to_start = dist(laptime_struct->gps_latitude_start, laptime_struct->gps_longitude_start,
												 laptime_struct->gps_current_latitude, laptime_struct->gps_current_longitude);
		laptime_struct->position_id++;
		check_for_start(laptime_struct);
	}
}

/**
 * @brief Helper funtion that sends the start position to CAN and by UDP (by raising flag)
 *
 * @param laptime_struct Pointer to laptime struct
 */
void send_start_position(laptime_t *laptime_struct)
{
	uint32_t laptime_uint32_lat = (uint32_t)(laptime_struct->gps_current_latitude * 10000000);
	uint32_t laptime_uint32_long = (uint32_t)(laptime_struct->gps_current_longitude * 10000000);

	laptime_struct->laptime_start_msg.data[0] = (uint8_t)(laptime_uint32_lat >> 24);
	laptime_struct->laptime_start_msg.data[1] = (uint8_t)(laptime_uint32_lat >> 16);
	laptime_struct->laptime_start_msg.data[2] = (uint8_t)(laptime_uint32_lat >> 8);
	laptime_struct->laptime_start_msg.data[3] = (uint8_t)(laptime_uint32_lat);

	laptime_struct->laptime_start_msg.data[4] = (uint8_t)(laptime_uint32_long >> 24);
	laptime_struct->laptime_start_msg.data[5] = (uint8_t)(laptime_uint32_long >> 16);
	laptime_struct->laptime_start_msg.data[6] = (uint8_t)(laptime_uint32_long >> 8);
	laptime_struct->laptime_start_msg.data[7] = (uint8_t)(laptime_uint32_long);

#ifdef BUILD_FLAG_REC
	twai_transmit(&(laptime_struct->laptime_start_msg), 10);
	laptime_struct->send_udp_start_flag = true;
#endif
}

/**
 * @brief Set the start of the lap according to the selected mode.
 * 
 * @param laptime_struct Pointer to laptime struct
 * @return true 
 * @return false 
 */
bool set_start(laptime_t *laptime_struct)
{
	switch (laptime_struct->laptime_mode)
	{
	case AUTOCROSS:
		/* set autocross start */
		laptime_struct->lap_info.autocross_start_latitude = laptime_struct->gps_current_latitude;
		laptime_struct->lap_info.autocross_start_longitude = laptime_struct->gps_current_longitude;
		//send_start_position(laptime_struct);
		printf("Autocross start set.\n");
		laptime_struct->start_is_set = true;

		break;
	case SKIDPAD:
		/* set skidpad start */
		laptime_struct->lap_info.skidpad_start_latitude = laptime_struct->gps_current_latitude;
		laptime_struct->lap_info.skidpad_start_longitude = laptime_struct->gps_current_longitude;
		//send_start_position(laptime_struct);

		printf("Skidpad start set.\n");

		laptime_struct->start_is_set = true;

		break;
	case ACCELERATION:
		/* set accel start */
		laptime_struct->lap_info.accel_start_latitude = laptime_struct->gps_current_latitude;
		laptime_struct->lap_info.accel_start_longitude = laptime_struct->gps_current_longitude;
		//send_start_position(laptime_struct);

		printf("Acceleration start set.\n");

		laptime_struct->start_is_set = true;
		break;

	case NO_MODE_SELECTED:
		break;

	default:
		break;
	}

	return true;
}

/**
 * @brief Resets the lap counters of all modes ie. if a new driver is driving/lap gets changed etc.
 * 
 * @param laptime_struct Pointer to laptime struct
 * @return true On success
 */
bool reset_lap_counter(laptime_t *laptime_struct)
{
	laptime_struct->lap_counter = 1;
	laptime_struct->lap_info.autocross_laps = 1;
	laptime_struct->lap_info.skidpad_laps = 1;
	laptime_struct->lap_info.skidpad_runs = 1;
	printf("Laps reset!\n");
	return true;
}

/**
 * @brief Sends the info about the last completed lap and raises a flag to send that information by UDP to the car
 * 
 * @param laptime_struct Pointer to laptime struct
 * @return true On success
 * @return false On failed transmit
 */
bool send_laptime_info(laptime_t *laptime_struct)
{
	switch (laptime_struct->laptime_mode)
	{
	case AUTOCROSS:
		// code
		laptime_struct->laptime_timer_info_msg.data_length_code = 4;
		laptime_struct->laptime_timer_info_msg.data[0] = laptime_struct->lap_info.autocross_laps;
		laptime_struct->laptime_timer_info_msg.data[1] = (uint8_t)(laptime_struct->lap_info.autocross_laptime >> 16);
		laptime_struct->laptime_timer_info_msg.data[2] = (uint8_t)(laptime_struct->lap_info.autocross_laptime >> 8);
		laptime_struct->laptime_timer_info_msg.data[3] = (uint8_t)(laptime_struct->lap_info.autocross_laptime);
		printf("AutoX Laptime: %i ms in lap %i\n", laptime_struct->lap_info.autocross_laptime, laptime_struct->lap_info.autocross_laps);
		break;
	case SKIDPAD:
		// code
		laptime_struct->laptime_timer_info_msg.data_length_code = 5;
		laptime_struct->laptime_timer_info_msg.data[0] = laptime_struct->lap_info.skidpad_laps;
		laptime_struct->laptime_timer_info_msg.data[1] = (uint8_t)(laptime_struct->lap_info.skidpad_laptime >> 16);
		laptime_struct->laptime_timer_info_msg.data[2] = (uint8_t)(laptime_struct->lap_info.skidpad_laptime >> 8);
		laptime_struct->laptime_timer_info_msg.data[3] = (uint8_t)(laptime_struct->lap_info.skidpad_laptime);
		laptime_struct->laptime_timer_info_msg.data[4] = laptime_struct->lap_info.skidpad_runs;
		printf("Skidpad Laptime: %i ms in lap %i\n", laptime_struct->lap_info.skidpad_laptime, laptime_struct->lap_info.skidpad_laps);
		printf("Skidpad Run: %i\n",laptime_struct->lap_info.skidpad_runs);
		break;
	case ACCELERATION:
		// code
		laptime_struct->laptime_timer_info_msg.data_length_code = 4;
		laptime_struct->laptime_timer_info_msg.data[0] = 0;
		laptime_struct->laptime_timer_info_msg.data[1] = (uint8_t)(laptime_struct->lap_info.accel_laptime >> 16);
		laptime_struct->laptime_timer_info_msg.data[2] = (uint8_t)(laptime_struct->lap_info.accel_laptime >> 8);
		laptime_struct->laptime_timer_info_msg.data[3] = (uint8_t)(laptime_struct->lap_info.accel_laptime);
		break;
	default:
		break;
	}

	if (twai_transmit(&laptime_struct->laptime_timer_info_msg, 10) == ESP_OK)
	{
		laptime_struct->send_udp_laptime_flag = true;
		return true;
	}
	return false;
}

/**
 * @brief Sends the configuration and the current timer if polled by command.
 * 
 * @param laptime_struct Pointer to laptime struct
 * @return true On success
 * @return false On failure to transmit
 */
bool send_laptime_status(laptime_t *laptime_struct)
{
	laptime_struct->laptime_status_msg.data_length_code = 6;
	laptime_struct->laptime_status_msg.data[0] = laptime_struct->laptime_mode;
	laptime_struct->laptime_status_msg.data[1] = laptime_struct->start_is_set;
	laptime_struct->laptime_status_msg.data[2] = laptime_struct->laptime_started;
	laptime_struct->laptime_status_msg.data[3] = (uint8_t)(laptime_struct->current_laptime >> 16);
	laptime_struct->laptime_status_msg.data[4] = (uint8_t)(laptime_struct->current_laptime >> 8);
	laptime_struct->laptime_status_msg.data[5] = (uint8_t)(laptime_struct->current_laptime);

	if (!twai_transmit(&laptime_struct->laptime_status_msg, 10) == ESP_OK)
	{
		return false;
	}
	return true;
}

/**
 * @brief Gets the current laptime and calls function to sends the status info
 * 
 * @param laptime_struct Pointer to laptime struct
 * @return true On success
 */
bool poll_status(laptime_t *laptime_struct)
{
	double laptime;

	timer_get_counter_time_sec(TIMER_GROUP_0, TIMER_0, &laptime);
	laptime_struct->current_laptime = (uint32_t)(laptime * 1000); // seconds to ms
	printf("Timer polled: %f\n",laptime);
	send_laptime_status(laptime_struct);
	return true;
}


/*
	switch (laptime_struct->laptime_mode)
	{
	case AUTOCROSS:

		// code
		break;

	case SKIDPAD:

		// code
		break;

	case ACCELERATION:

		// code
		break;


	default:
		break;
	}
*/