/**
 * @dir lib/Laptime
 *
 * @brief Source and header files for laptime functionality using GPS position
 *
 * # Overview
 * By saving a GPS position as a start and then checking the distance between the current position and the starting position
 * a lap can be defined. When setting a start a timer is startet that times the lap. After a short lockout based on number
 * of positions a lab can be recognized.
 * This is done by first checking if the distance is inside a safe zone and then checking
 * the distance until it gets bigger again. The timer value gets read and sent by CAN and UDP with the lab number.
 *
 * 
 * 
 * ## Working with the timer
 * 
 * In the following table the command structure can be seen.
 * The ID of the CAN Message used for the commands is 0x22D
 * 
 * | **Byte** | **Command**       | **Value** | **Action**                       |
 * |----------|-------------------|-----------|----------------------------------|
 * | 0        | Select Mode       | 0         | -> nothing                       |
 * |          |                   | 1         | -> AutoX                         |
 * |          |                   | 2         | -> Skidpad                       |
 * |          |                   | 3         | -> Acceleration                  |
 * |          |                   |           |                                  |
 * | 1        | Set start         | 0         | -> nothing                       |
 * |          |                   | 1         | -> reset start                   |
 * |          |                   | 2         | -> set start                     |
 * |          |                   |           |                                  |
 * | 2        | Start timer       | 0         | -> nothing                       |
 * |          |                   | 1         | -> stop timer                    |
 * |          |                   | 2         | -> start timer                   |
 * |          |                   |           |                                  |
 * | 3        | Reset laps        | 0         | -> nothing                       |
 * |          |                   | 1         | -> reset lap counter             |
 * |          |                   |           |                                  |
 * | 4        | Change start area | 0         | -> nothing                       |
 * |          |                   | x         | -> set start area to x           |
 * |          |                   |           |                                  |
 * | 5        | Poll sttus        | 0         | ->nothing                        |
 * |          |                   | 1         | -> poll current status and timer |
 * |          |                   |           |                                  |
 * 
 * @file laptime.h
 * @author Clemens Jensen (clemens_jensen@yahoo.com)
 * @brief
 * @version 0.1
 * @date 2022-07-26
 *
 * @copyright Copyright (c) 2022
 *
 */
#ifndef LAPTIME_H
#define LAPTIME_H

#include "stdint.h"
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <stdbool.h>

#include "driver/timer.h"
#include "driver/twai.h"
#include "gps.h"

/// From util.h to convert bytes of a can message into uint16
#define BYTES_TO_U16(b1, b2)                         \
    ((uint16_t)((((uint16_t)((uint8_t)(b1))) << 8) | \
                (uint16_t)((uint8_t)(b2))))

/// From util.h to convert bytes of a can message into uint32
#define BYTES_TO_U32(b1, b2, b3, b4)                  \
    ((uint32_t)((((uint32_t)((uint8_t)(b1))) << 24) | \
                (((uint32_t)((uint8_t)(b2))) << 16) | \
                (((uint32_t)((uint8_t)(b3))) << 8) |  \
                (uint32_t)((uint8_t)(b4))))

/// Radius of the earth in m
#define R 6371000
/// Converting factor for deg -> rad
#define TO_RAD (3.1415926536 / 180)
///  Hardware timer clock divider
#define TIMER_DIVIDER (16)
/// convert counter value to seconds
#define TIMER_SCALE (TIMER_BASE_CLK / TIMER_DIVIDER)

/**
 * @brief Enum for setting the mode of the laptimer
 *
 *
 *
 */
typedef enum laptime_mode
{
    NO_MODE_SELECTED = 0, /**< Laptime mode: no mode selected*/
    AUTOCROSS,            /**< Laptime mode: autocross*/
    SKIDPAD,              /**< Laptime mode: skidpad*/
    ACCELERATION,         /**< Laptime mode: acceleration (currently not used)*/

} laptime_mode_t;

/**
 * @brief Starting points of different dynamic disciplines
 *
 */
typedef struct lap_info_struct
{
    float autocross_start_latitude;  /**<  Latitude of Autocross starting area */
    float autocross_start_longitude; /**<  Longitude of Autocross starting area */
    int autocross_laps;            /**<  Number of laps driven in a given autocross testing procedure */
    uint32_t autocross_laptime;        /**<  Laptime of the last driven Autocross lap */

    float skidpad_start_latitude;  /**<  Latitude of Skidpad starting area */
    float skidpad_start_longitude; /**<  Longitude of Skidpad starting area */
    int skidpad_laps;            /**<  Number of the lap in a skidpad run */
    int skidpad_runs;              /**< Number of the current run of a skidpad test  */
    uint32_t skidpad_laptime;        /**<  Laptime of the last driven Skidpad lap */

    float accel_start_latitude;  /**<  Latitude of Accel starting area */
    float accel_start_longitude; /**<  Longitude of Accel starting area */
    uint32_t accel_laptime;         /**<  Time of an accel run */
} lap_info_t;

/**
 * @brief Struct used in every laptime function.
 *
 */
typedef struct laptime_struct
{
    laptime_mode_t laptime_mode;            /**< Mode of the laptimer to change behaviour*/
    lap_info_t lap_info;                    /**< Info of different laptimes given in lap_info_struct*/
    float gps_current_latitude;             /**< Current latitude of the car */
    float gps_current_longitude;            /**< Current longitude of the car */
    float gps_latitude_start;               /**< Latitude of the start */
    float gps_longitude_start;              /**< Longitude of the start */
    float distance_to_start;                /**< Current distance to the starting position*/
    float previous_distance_to_start;       /**< Distance n-1 to start (used for determining start crossing) */
    int lap_counter;                        /**< Counter for laps started with laptimer*/
    uint32_t current_laptime;               /**< Current laptime for usage in cases where mode is not important (use lap_info struct otherwise)*/
    bool start_is_set;                      /**< Flag whether a start is set*/
    bool laptime_started;                   /**< Flag whether laptimer is started*/
    int position_id;                        /**< Counter for position data points used to not immediately detect a new lap when crossing start */
    bool send_udp_laptime_flag;             /**< Flag that enables udp sending of current laptime/lap number message*/
    bool send_udp_start_flag;               /**< Flag that enable UDP sending of start position*/
    int start_zone;                         /**< Zone in which a start can be detected */
    twai_message_t laptime_timer_info_msg;  /**< CAN Message containing laptime info (lap/time) */
    twai_message_t laptime_start_msg;       /**< CAN Message containing start position information*/
    twai_message_t laptime_status_msg;      /**< CAN Message containing status of laptimer and current timer value */
} laptime_t;

void init_laptimer(int group, int timer);

float dist(float start_lat, float start_lon, float lat, float lon);

void convert_message(twai_message_t position_message, laptime_t *laptime_struct);

bool check_for_start(laptime_t *laptime_struct);

void start_laptime(laptime_t *laptime_struct);

bool stop_laptime(laptime_t *laptime_struct);

uint32_t get_laptime_value(laptime_t *laptime_struct);

void handle_commands(twai_message_t can_message, laptime_t *laptime_struct);

void uart_laptime(float latitude, float longitude, laptime_t *laptime_struct);

void send_start_position(laptime_t *laptime_struct);

bool set_start(laptime_t *laptime_struct);

bool reset_lap_counter(laptime_t *laptime_struct);

bool send_laptime_info(laptime_t *laptime_struct);

bool get_current_laptime(laptime_t *laptime_struct);

bool poll_status(laptime_t *laptime_struct);

#endif /* LAPTIME_H */