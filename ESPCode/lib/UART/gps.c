/**
 * @file gps.c
 * @author Clemens Jensen (clemens_jensen@yahoo.com)
 * 
 * @details Source Code for GPS functionality.
 *
 * 
 * - Port: UART2
 * - Receive (Rx) buffer: on
 * - Transmit (Tx) buffer: off
 * - Flow control: off
 * - Event queue: on
 * - Pin assignment: TxD (25), RxD (26)
 *
 * @version 0.1
 * @date 2022-05-12
 *
 * @copyright Copyright (c) 2022
 */

#include "gps.h"
#include "driver/twai.h"

#define EX_UART_NUM UART_NUM_2 /*!< Set UART interface to number 2 (of 3)*/
#define PATTERN_CHR_NUM (1)    /*!< Set the number of consecutive and identical characters received by receiver which defines a UART pattern*/

#define BUF_SIZE (1024)        /*!< Set buffer size to 1024 bytes*/
#define RD_BUF_SIZE (BUF_SIZE) /*!< Set read buffer to 1024 bytes*/

static const char *GPS_TAG = "GPS";

twai_message_t tx_message_lat_long;
twai_message_t tx_message_speed;
int new_gps_messages_flag;

extern laptime_t laptime_struct;

/**
 * @brief Function sends can messages containing gps data to can bus.
 * 
 * Populates the appropiate can messages with data from esp_gps_t and then sends them to the bus.
 * Only sends data, when there is a gps fix and there is actual data to be transmitted.
 * 
 * @param esp_gps Gps runtime struct. Contains parsed gps data.
 * 
 */
void send_can(esp_gps_t *esp_gps)
{
    /* Message structure for latitude and logitude*/

    tx_message_lat_long.identifier = 554;
    tx_message_lat_long.data_length_code = 8;

    tx_message_lat_long.data[0] = (uint8_t)(esp_gps->parent.latitude_uint32 >> 24);
    tx_message_lat_long.data[1] = (uint8_t)(esp_gps->parent.latitude_uint32 >> 16);
    tx_message_lat_long.data[2] = (uint8_t)(esp_gps->parent.latitude_uint32 >> 8);
    tx_message_lat_long.data[3] = (uint8_t)(esp_gps->parent.latitude_uint32);

    tx_message_lat_long.data[4] = (uint8_t)(esp_gps->parent.longitude_uint32 >> 24);
    tx_message_lat_long.data[5] = (uint8_t)(esp_gps->parent.longitude_uint32 >> 16);
    tx_message_lat_long.data[6] = (uint8_t)(esp_gps->parent.longitude_uint32 >> 8);
    tx_message_lat_long.data[7] = (uint8_t)(esp_gps->parent.longitude_uint32); 

    tx_message_speed.identifier = 555;
    tx_message_speed.data_length_code = 6;

    tx_message_speed.data[0] = (uint8_t)(esp_gps->parent.speed_uint16 >> 8);
    tx_message_speed.data[1] = (uint8_t)(esp_gps->parent.speed_uint16);

    tx_message_speed.data[2] = (uint8_t)(esp_gps->parent.speed_kph_uint16 >> 8);
    tx_message_speed.data[3] = (uint8_t)(esp_gps->parent.speed_kph_uint16);

    tx_message_speed.data[4] = (uint8_t)(esp_gps->parent.cog_uint16 >> 8);
    tx_message_speed.data[5] = (uint8_t)(esp_gps->parent.cog_uint16);

    /*
     * Only transmit position information when the gps module has a fix.
     */
    if (tx_message_lat_long.data[0] != 0)
    {
        twai_transmit(&tx_message_lat_long, pdMS_TO_TICKS(1)); // try sending can message for 1ms
        twai_transmit(&tx_message_speed, pdMS_TO_TICKS(1));    // try sending can message for 1ms
        new_gps_messages_flag = 1;
    }
}
/**
 * @brief Converts latitude and longitude strings to a float number.
 * The return value is the position in decimal degrees-
 * 
 * @param esp_gps Runtime structure containing gps data.
 * @return float 
 */
float parse_lat_long(esp_gps_t *esp_gps)
{
    float ll = strtof(esp_gps->item_str, NULL);
    int deg = ((int)ll) / 100;
    float min = ll - (deg * 100);
    ll = deg + min / 60.0f;
    return ll;
}
/**
 * @brief Function switches cases for different RMC fields and saves data into esp_gps_t.parent (gps_t).
 * 
 * @param esp_gps Runtime structure containing esp_gps_t.item_num for current item and esp_gps_t.parent for data.
 */
void parse_rmc(esp_gps_t *esp_gps)
{
    switch (esp_gps->item_num)
    {
    case 1: /* Parse UTC time*/
        // printf("1: %s\n",esp_gps->item_str);
        break;
    case 2: /* Process valid status */
        esp_gps->parent.valid = (esp_gps->item_str[0] == 'A');
        break;
    case 3: /* Latitude */
        esp_gps->parent.latitude_f = parse_lat_long(esp_gps);
        esp_gps->parent.latitude_uint32 = (uint32_t)(esp_gps->parent.latitude_f * 10000000);
        break;
    case 4: /* Latitude north(1)/south(-1) information */
        if (esp_gps->item_str[0] == 'S' || esp_gps->item_str[0] == 's')
        {
            esp_gps->parent.latitude_f *= -1;
        }
        break;
    case 5: /* Longitude */
        esp_gps->parent.longitude_f = parse_lat_long(esp_gps);
        esp_gps->parent.longitude_uint32 = (uint32_t)(esp_gps->parent.longitude_f * 10000000);
        break;
    case 6: /* Longitude east(1)/west(-1) information */
        if (esp_gps->item_str[0] == 'W' || esp_gps->item_str[0] == 'w')
        {
            esp_gps->parent.longitude_f *= -1;
        }
        break;
    case 7: /* Process ground speed in unit m/s */
        esp_gps->parent.speed_f = strtof(esp_gps->item_str, NULL) * 0.5144;
        esp_gps->parent.speed_kph_f = esp_gps->parent.speed_f * 3.6;
        esp_gps->parent.speed_uint16 = (uint16_t)(esp_gps->parent.speed_f * 1000);
        esp_gps->parent.speed_kph_uint16 = (uint16_t)(esp_gps->parent.speed_kph_f * 100);
        break;
    case 8: /* Process true course over ground */
        esp_gps->parent.cog = strtof(esp_gps->item_str, NULL);
        esp_gps->parent.cog_uint16 = (uint16_t)(esp_gps->parent.cog * 100);
        break;
    case 9: /* Process date */
        break;
    case 10: /* Process magnetic variation */
        // printf("10: %s\n",esp_gps->item_str);
        break;
    default:
        break;
    }
}

/**
 * @brief Checks what kind of item needs to be parsed. If the item is *not* the first one in the current statement there
 * is actual RMC data to be parsed. Calls parse_rmc() in that case.
 * 
 * @todo Implement other NMEA statements. Currently only RMC is send by GNSS module as it is all the information we need.
 * (position, speed and heading)
 * 
 * @param esp_gps Runtime structure containing current item. @see esp_gps_t.item_str
 * @return esp_err_t 
 */
esp_err_t parse_item(esp_gps_t *esp_gps)
{
    esp_err_t err = ESP_OK;
    /* Start of a statement*/
    if (esp_gps->item_num == 0 && esp_gps->item_str[0] == '$')
    {
        /* code */
    }
    else
    {
        parse_rmc(esp_gps);
    }
    return err;
}
/**
 * @brief Decodes gps data from nmea statement.
 * 
 * @param esp_gps Runtime structure containing received char array from GNSS module. @see esp_gps_t.buffer
 * @param len Length of received char array. Currently unused.
 * @return esp_err_t 
 */
esp_err_t gps_decode(esp_gps_t *esp_gps, size_t len)
{
    const uint8_t *d = esp_gps->buffer;
    while (*d)
    {
        if (*d == '$')
        {
            /* Reset runtime information */
            esp_gps->asterisk = 0;
            esp_gps->item_num = 0;
            esp_gps->item_pos = 0;
            esp_gps->crc = 0;
            /* */
            esp_gps->item_str[esp_gps->item_pos++] = *d;
            esp_gps->item_str[esp_gps->item_pos] = '\0';
        }
        else if (*d == ',')
        {
            /* Parse current item*/
            parse_item(esp_gps);
            /* Add character to CRC calculation*/
            esp_gps->crc ^= (uint8_t)(*d);
            /* Start with next item */
            esp_gps->item_pos = 0;
            esp_gps->item_str[0] = '\0';
            esp_gps->item_num++;
        }
        else if (*d == '*')
        {
            /* Parse current item */
            parse_item(esp_gps);
            /* Asterisk detected */
            esp_gps->asterisk = 1;
            /* Start with next item */
            esp_gps->item_pos = 0;
            esp_gps->item_str[0] = '\0';
            esp_gps->item_num++;
        }
        else if (*d == '\n')
        {
            uint8_t crc = (uint8_t)strtol(esp_gps->item_str, NULL, 16);
            /* CRC passed */
            if (esp_gps->crc == crc)
            {
                send_can(esp_gps);
            }
            else
            {
                ESP_LOGD(GPS_TAG, "CRC Error!");
            }
        }
        else
        {
            if (!(esp_gps->asterisk))
            {
                /* Add to checksum */
                esp_gps->crc ^= (uint8_t)(*d);
            }
            /* Add character to item string */
            // printf("%2x",*d);
            esp_gps->item_str[esp_gps->item_pos++] = *d;
            esp_gps->item_str[esp_gps->item_pos] = '\0';
        }
        d++;
    }
    return ESP_OK;
}
/**
 * @brief Handles uart event (nmea statement received). Prepares string for further usage.
 * 
 * @param esp_gps Runtime structure for gps.
 */
void esp_handle_uart_pattern(esp_gps_t *esp_gps)
{
    int pos = uart_pattern_pop_pos(esp_gps->uart_port);
    if (pos != -1)
    {
        /* Read one line(include '\n') until pattern position + 1 is reached */
        int read_len = uart_read_bytes(esp_gps->uart_port, esp_gps->buffer, pos + 1, 100 / portTICK_PERIOD_MS);
        /* Make sure the line is a standard string */
        esp_gps->buffer[read_len] = '\0';
        /* Send new line to handle */
        if (gps_decode(esp_gps, read_len + 1) != ESP_OK)
        {
            ESP_LOGW(GPS_TAG, "GPS decode line failed");
        }
    }
    else
    {
        ESP_LOGW(GPS_TAG, "Pattern Queue Size too small");
        uart_flush_input(esp_gps->uart_port);
    }
}

/**
 *
 * @brief RTOS task to handle uart events.
 *
 * @param pvParameters
 */
void gps_parser_task(void *arg)
{
    esp_gps_t *esp_gps = (esp_gps_t *)arg;
    uart_event_t event;
    while (1)
    {
        if (xQueueReceive(esp_gps->event_queue, &event, pdMS_TO_TICKS(200)))
        {
            // ESP_LOGI(UART_TAG, "uart[%d] event:", EX_UART_NUM);
            switch (event.type)
            {
            case UART_DATA:
                break;
            case UART_FIFO_OVF:
                ESP_LOGW(GPS_TAG, "HW FIFO Overflow");
                uart_flush(esp_gps->uart_port);
                xQueueReset(esp_gps->event_queue);
                break;
            case UART_BUFFER_FULL:
                ESP_LOGW(GPS_TAG, "Ring Buffer Full");
                uart_flush(esp_gps->uart_port);
                xQueueReset(esp_gps->event_queue);
                break;
            case UART_BREAK:
                ESP_LOGW(GPS_TAG, "Rx Break");
                break;
            case UART_PARITY_ERR:
                ESP_LOGE(GPS_TAG, "Parity Error");
                break;
            case UART_FRAME_ERR:
                ESP_LOGE(GPS_TAG, "Frame Error");
                break;
            case UART_PATTERN_DET:
                esp_handle_uart_pattern(esp_gps);
                break;
            default:
                ESP_LOGW(GPS_TAG, "Unknown UART event: %d", event.type);
                break;
            }
        }
    }
    vTaskDelete(NULL);
}
/**
 * @brief Configures UART driver, allocates memory for runtime structures. Also starts the RTOS task.
 * 
 * @param config 
 */
void gps_init(const gps_parser_config_t *config)
{
    /* allocate memory for esp_gps struct*/
    esp_gps_t *esp_gps = calloc(1, sizeof(esp_gps_t));
    if (!esp_gps)
    {
        ESP_LOGE(GPS_TAG, "calloc memory for esp_fps failed");
    }
    esp_gps->buffer = calloc(1, 1024 / 2);
    if (!esp_gps->buffer)
    {
        ESP_LOGE(GPS_TAG, "calloc memory for runtime buffer failed");
    }
    /* Set attributes of esp_gps*/
    esp_gps->uart_port = config->uart.uart_port;
    /* Configure parameters of an UART driver,
     * communication pins and install the driver */
    uart_config_t uart_config = {
        .baud_rate = config->uart.baud_rate,
        .data_bits = config->uart.data_bits,
        .parity = config->uart.parity,
        .stop_bits = config->uart.stop_bits,
        .flow_ctrl = UART_HW_FLOWCTRL_DISABLE,
        .source_clk = UART_SCLK_APB,
    };
    /* Install UART driver and get queue */
    if (uart_driver_install(esp_gps->uart_port, CONFIG_GPS_PARSER_RING_BUFFER_SIZE, 0,
                            config->uart.event_queue_size, &esp_gps->event_queue, 0) != ESP_OK)
    {
        ESP_LOGE(GPS_TAG, "Installation of UART driver failed.");
        uart_driver_delete(esp_gps->uart_port);
    }
    if (uart_param_config(esp_gps->uart_port, &uart_config) != ESP_OK)
    {
        ESP_LOGE(GPS_TAG, "Configuration of UART parameters failed.");
    }
    /* Set UART pins (using pin 25 and 26 with default flow control pins) */
    if (uart_set_pin(esp_gps->uart_port, config->uart.tx_pin, config->uart.rx_pin,
                     UART_PIN_NO_CHANGE, UART_PIN_NO_CHANGE) != ESP_OK)
    {
        ESP_LOGE(GPS_TAG, "Configuration of UART GPIO failed.");
    }
    /* Set pattern interrupt, used to detect the EOL */
    uart_enable_pattern_det_baud_intr(esp_gps->uart_port, '\n', 1, 9, 0, 0);
    /* Set pattern queue size to record at most 20 pattern positions. */
    uart_pattern_queue_reset(esp_gps->uart_port, config->uart.event_queue_size);
    uart_flush(esp_gps->uart_port);

    /* Create GPS Parser task */
    BaseType_t err = xTaskCreate(gps_parser_task,
                                 "gps_parser",
                                 CONFIG_GPS_PARSER_TASK_STACK_SIZE,
                                 esp_gps,
                                 CONFIG_GPS_PARSER_TASK_PRIORITY,
                                 &esp_gps->tsk_hdl);
    if (err != pdTRUE)
    {
        ESP_LOGE(GPS_TAG, "Creation of GPS Parser Task failed.");
    }
    return;
}