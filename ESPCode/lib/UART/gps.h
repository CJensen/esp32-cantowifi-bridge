/**
 * @dir lib/UART
 *
 * @brief Source and header files for gps module functionality
 * 
 * # Overview
 * The telemetry module uses a uBlox GNSS module to gather position and velocity data of the racecar.
 * The ESP and the GNSS module communicate by UART. NMEA statements sent by the module are parsed and saved into
 * a struct (gps_t) for further uses like sending them over WiFi/CAN.
 * 
 * The GNSS module uses [NMEA0183](https://en.wikipedia.org/wiki/NMEA_0183) statements for communicating the data.
 * 
 * A standard NMEA statement looks like this: `$GNRMC,114814.00,A,5420.56193,N,01008.32902,E,0.419,,130521,,,A*67`
 * 
 * There are different message types denoted by the first field after the $. In the above case the message is RMC 
 * – Recommended minimum specific GNSS data.
 * Each field seperated by a comma contains specific data. for example the 4th field, 5420.56193, contains the Latitude.
 * 
 * ![RMC message content](RMC-statement.png)
 * 
 * The program writes the statement to the esp_gps_t.buffer. The buffer is then parsed char by char by the gps_decode()
 * function either until a field delimiter (,) or an asterisk is found. Each time this happens the checksum is updated.
 * 
 * When the parsed char is a comma the function parse_item() is called.
 * When the current item is actual data (not $GNRMC), parse_rmc()
 * is called. This function then has a switch case mapped to the NMEA statement fields extracting the data from the current field.
 * The current field is tracked in esp_gps_t.item_num.
 *
 * When the parsed char is an asterisk the last field was parsed and the esp_gps_t.asterisk flag is raised.
 * This leads to the checksum getting checked and send_can() is called to send gps data to can bus.
 * 
 * 
 * 
 * @file gps.h
 * @author Clemens Jensen (clemens_jensen@yahoo.com)
 * @brief
 * @version 0.1
 * @date 2022-05-12
 *
 * @copyright Copyright (c) 2022
 *
 */
#ifndef UART_H
#define UART_H

#include <stdio.h>
#include <string.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/queue.h"
#include "esp_log.h"
#include "esp_types.h"
#include "esp_event.h"
#include "esp_err.h"
#include "driver/uart.h"
#include "laptime.h"

/// Maximum length of each NMEA item (comma seperated) in a message.
#define NMEA_MAX_STATEMENT_ITEM_LENGTH (16)
/// Size of the event loop queue.
#define NMEA_EVENT_LOOP_QUEUE_SIZE (16)
/// Default configuration macro for NMEA Parser.
#define GPS_PARSER_CONFIG_DEFAULT()             \
    {                                           \
        .uart = {.uart_port = UART_NUM_2,       \
                 .rx_pin = 26,                  \
                 .tx_pin = 25,                  \
                 .baud_rate = 9600,             \
                 .data_bits = UART_DATA_8_BITS, \
                 .parity = UART_PARITY_DISABLE, \
                 .stop_bits = UART_STOP_BITS_1, \
                 .event_queue_size = 20         \
        }                                       \
    }

/**
 * @brief GPS data structure
 *
 */
typedef struct
{
    float latitude_f;          /*!< Raw Latitude (decimal degrees) */
    float longitude_f;         /*!< Raw Longitude (decimal degrees) */
    uint32_t latitude_uint32;  /*!< Latitude in uint32 */
    uint32_t longitude_uint32; /*!< Longitude in uint32 */
    float speed_f;             /*!< Ground Speed, unit: m/s */
    uint16_t speed_uint16;     /*!< Gound Speed in uint16 */
    float speed_kph_f;         /*!< Ground Speed, unit: kph */
    uint16_t speed_kph_uint16; /*!< Gound Speed in uint16 */
    bool valid;                /*!< Valid data */
    float cog;                 /*!< True course over ground */
    uint16_t cog_uint16;
} gps_t;

/**
 * @brief GPS runtime structure
 *
 */
typedef struct
{
    uint8_t *buffer;                               /*!< Runtime buffer for full NMEA messages */
    uint8_t item_num;                              /*!< Position of current item */
    uint8_t item_pos;                              /*!< Position inside the current item */
    uint8_t asterisk;                              /*!< Last char before CRC */
    uint8_t crc;                                   /*!< Checksum */
    uart_port_t uart_port;                         /*!< UART port used for GNSS receiver */
    gps_t parent;                                  /*!< GPS data parsed from NMEA0183 statements */
    QueueHandle_t event_queue;                     /*!< UART event queue for received NMEA messages */
    TaskHandle_t tsk_hdl;                          /*!< RTOS task handle */
    char item_str[NMEA_MAX_STATEMENT_ITEM_LENGTH]; /*!< String containing current item from NMEA statement */
} esp_gps_t;

/**
 * @brief Configuration of UART
 *
 */
typedef struct
{
    struct
    {
        uart_port_t uart_port;        /*!< UART port number */
        uint32_t rx_pin;              /*!< UART Rx Pin number */
        uint32_t tx_pin;              /*!< UART Tx Pin number */
        uint32_t baud_rate;           /*!< UART baud rate */
        uart_word_length_t data_bits; /*!< UART data bits length */
        uart_parity_t parity;         /*!< UART parity */
        uart_stop_bits_t stop_bits;   /*!< UART stop bits length */
        uint32_t event_queue_size;    /*!< UART event queue size */
    } uart;
} gps_parser_config_t;

void send_can(esp_gps_t *esp_gps);

float parse_lat_long(esp_gps_t *esp_gps);

void parse_rmc(esp_gps_t *esp_gps);

esp_err_t parse_item(esp_gps_t *esp_gps);

esp_err_t gps_decode(esp_gps_t *esp_gps, size_t len);

void esp_handle_uart_pattern(esp_gps_t *esp_gps);

void gps_parser_task(void *arg);

void handle_uart_pattern(esp_gps_t *esp_gps);

void gps_init(const gps_parser_config_t *config);

#endif /* UART_H */