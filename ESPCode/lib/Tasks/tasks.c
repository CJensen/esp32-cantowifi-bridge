/**
 * @file tasks.c
 * @author Clemens Jensen (clemens_jensen@yahoo.com)
 * 
 * @brief
 * @version 0.1
 * @date 2022-05-17
 * 
 * @copyright Copyright (c) 2022
 * 
 */
#include "tasks.h"
#include "laptime.h"
// ESP LOGGING
static const char *TAG_UDP = "UDP";

extern twai_message_t tx_message_lat_long;
extern twai_message_t tx_message_speed;
extern int new_gps_messages_flag;

extern laptime_t laptime_struct;

/**
 * @brief Function for converting CAN messages to UDP packets and sending them.
 * 
 * @todo Implement timer instead of j < 100 condition. This is needed right now because else the text interface messages will not
 * be sent in a timely manner.
 */
void task_CAN_to_UDP()
{
    vTaskDelay(pdMS_TO_TICKS(2500));
    twai_message_t rxmessage; // Initialze can receive message.
    #ifdef BUILD_FLAG_REC
    init_laptimer(TIMER_GROUP_0, TIMER_0);
    #endif
    // IPV4 is used -> set adresses accordingly
    struct sockaddr_in dest_addr;
    dest_addr.sin_addr.s_addr = inet_addr(TRANSMIT_ADDR);
    dest_addr.sin_family = AF_INET;
    dest_addr.sin_port = htons(PORT);

    // Create udp (SOCK_DGRAM) socket IPv4
    int sock = socket(AF_INET, SOCK_DGRAM, 0);

    if (sock < 0) // Check whether socket creation was successful
    {
        ESP_LOGE(TAG_UDP, "Unable to create socket: errno %d", errno);
        esp_restart();
    }
    ESP_LOGI(TAG_UDP, "Socket created, sending to %s:%d", TRANSMIT_ADDR, PORT);

    while (true) // Infinite loop for RTOS task
    {
        while (sock > 0) // socket is running
        {
            int maxMessageLenght = 1400; // UDP packet size
            int position = 0;            // Position for can_message to be copied to
            char udp_packet[1400] = {};  // Empty char array for UDP packet
            int j = 0;
            // TODO: remove j condition and replace with timer so that TextInterface still works
            while ((position + 12 < maxMessageLenght) && (j < 100)) // Check whether next message copied to packet would exceed size. Maximum CAN Message size -> 12 Bytes
            {
                if (new_gps_messages_flag == 1 && ENABLE_GPS)
                {
                    add_message_to_packet(udp_packet,&position,tx_message_lat_long);
                    add_message_to_packet(udp_packet,&position,tx_message_speed);
                }

                if (laptime_struct.send_udp_laptime_flag)
                {
                    /* code */
                    add_message_to_packet(udp_packet,&position,laptime_struct.laptime_timer_info_msg);
                    laptime_struct.send_udp_laptime_flag = false;
                }
                

                if (twai_receive(&rxmessage, pdMS_TO_TICKS(10)) == ESP_OK) // Receive CAN message, wait 1s for a new message before failing.
                {
                    add_message_to_packet(udp_packet,&position,rxmessage);

                    #ifdef BUILD_FLAG_REC
                        handle_commands(rxmessage,&laptime_struct);
                    #endif
                }
                else // No message received for 1 second.
                {
                    j = j + 120;
                    //ESP_LOGE(TAG_CAN, "No message received for 1s.");
                }

            }
            j = 0;
            int err = sendto(sock, udp_packet, sizeof(udp_packet), 0, (struct sockaddr *)&dest_addr, sizeof(dest_addr)); // Send UDP packet to dest_addr
            if (err < 0)
            {
                // Note:
                // When the sending fails alot because of errno 12: ENOMEM make sure to connect with a station
                // That should fix the problem.
                //ESP_LOGI(TAG_UDP, "Error occurred during sending: errno %d", errno);
                err = sendto(sock, udp_packet, sizeof(udp_packet), 0, (struct sockaddr *)&dest_addr, sizeof(dest_addr));
            }
        }
    }
    vTaskDelete(NULL); // API call to delete task should it return
}

/**
 * @brief Function for converting UDP packets to CAN messages.
 * 
 */
void task_UDP_to_CAN()
{
    char rx_buffer[1400];

    twai_message_t txmessage;

    struct sockaddr_in dest_addr;
    // TODO:#ifdef hinzufügen
    dest_addr.sin_addr.s_addr = inet_addr(RECEIVE_ADDR); // (CAR "192.168.4.1" -> Laptop "192.168.4.2")
    dest_addr.sin_family = AF_INET;
    dest_addr.sin_port = htons(PORT); // 45500

    while (1)
    {
        int sock = socket(AF_INET, SOCK_DGRAM, 0);
        if (sock < 0)
        {
            ESP_LOGE(TAG_UDP, "Unable to create socket");
            break;
        }
        else
        {
            ESP_LOGI(TAG_UDP, "Socket created, sending to %s:%d", RECEIVE_ADDR, PORT);
        }

        int err = bind(sock, (struct sockaddr *)&dest_addr, sizeof(dest_addr));
        if (err < 0)
        {
            ESP_LOGE(TAG_UDP, "Socket unable to bind: errno %d", errno);
        }
        else
        {
            ESP_LOGI(TAG_UDP, "Socket bound, port %d", PORT);
        }
        while (1)
        {
            struct sockaddr_in6 source_addr; // Large enough for both IPv4 and IPv6
            socklen_t socklen = sizeof(source_addr);

            int len = recvfrom(sock, rx_buffer, sizeof(rx_buffer), 0, (struct sockaddr *)&source_addr, &socklen);
            if (len < 0)
            {
                ESP_LOGE(TAG_UDP, "recvfrom failed: errno %d", errno);
                break;
            }
            else
            {
                // TODO: Rewrite/Check function. Most messages were discarded due to len % NMessages == 0
                int startOfMessage;
                if (len >= 1)
                {
                    startOfMessage = 0;
                    while (rx_buffer[startOfMessage] == 0xFF)
                    {
                        if (rx_buffer[startOfMessage + 0] != 0xFF)
                        {
                            printf("Break reached!\n");
                            break;
                        }
                        txmessage.identifier = rx_buffer[startOfMessage + 1] << 8 | rx_buffer[startOfMessage + 2];
                        txmessage.data_length_code = rx_buffer[startOfMessage + 3];

                        int startOfData = startOfMessage + 4;

                        for (int i = startOfMessage; i < startOfMessage + txmessage.data_length_code; i++)
                        {
                            txmessage.data[i - startOfMessage] = (char)rx_buffer[i + 4];
                        }

                        startOfMessage = startOfData + txmessage.data_length_code;
                        
                        twai_transmit(&txmessage, portMAX_DELAY);
                        #ifdef BUILD_FLAG_REC
                            handle_commands(txmessage,&laptime_struct);
                        #endif
                    }
                }
            }
        }
    }
    vTaskDelete(NULL);
}

/**
 * @brief Initializes the CAN part of the program with a baudrate of 1 Mbit/s
 * 
 */
void init_can(void)
{
    // SETUP CAN/TWAI (Two Wire Automotive Interface)
    // Initialize configuration structures using macro initializers
    twai_general_config_t g_config = TWAI_GENERAL_CONFIG_DEFAULT(GPIO_NUM_21, GPIO_NUM_22, TWAI_MODE_NORMAL); // General config where CAN RX/TX pins and CAN MODE is chosen
    twai_timing_config_t t_config = TWAI_TIMING_CONFIG_1MBITS();                                              // Macro for setting can timing registers to work with 1MBit bus
    twai_filter_config_t f_config = TWAI_FILTER_CONFIG_ACCEPT_ALL();                                          // Setup filter to accept all can messages

    // Install CAN driver with configuration set beforehand
    if (twai_driver_install(&g_config, &t_config, &f_config) == ESP_OK)
    {
        printf("Driver installed\n");
    }
    else
    {
        printf("Failed to install driver\n");
        esp_restart();
        return;
    }

    // Start CAN driver
    if (twai_start() == ESP_OK)
    {
        printf("Driver started\n"); // Success starting CAN driver
    }
    else
    {
        printf("Failed to start driver\n"); // Failed to start CAN driver.
        return;
    }
}

/**
 * @brief Like strcat but is able to copy bytes that are 0
 *
 * @param s1 Buffer array to be copied to
 * @param n1 Size of array already in memory location. (Position to copy s2 to)
 * @param s2 Array that will be copied to s1
 * @param n2 Size of array s2
 * @return void*
 */
void *memcat(void *s1, size_t n1, void *s2, size_t n2)
{
    void *target = (char *)s1 + n1;
    memcpy(target, s2, n2);
    return s1;
}

/**
 * @brief Adds a single can message to the udp packet at the given position.
 * 
 * @param udp_packet UDP Packet consisting of converted can messages.
 * @param position Position the can message is copied to.
 * @param can_message CAN message that will be added to the packet.
 * @return void* 
 */
void* add_message_to_packet(char* udp_packet, int* position, twai_message_t can_message)
{
	char gps_lat_message[4 + can_message.data_length_code];
	gps_lat_message[0] = (char) 0xff;
	gps_lat_message[1] = (char)(can_message.identifier >> 8); 
	gps_lat_message[2] = (char)can_message.identifier;        
	gps_lat_message[3] = (char)can_message.data_length_code; 
	
	for (int i = 0; i < can_message.data_length_code; i++)
	{
		gps_lat_message[i + 4] = (char)can_message.data[i];
	}

	memcat(udp_packet, *position, gps_lat_message, sizeof(gps_lat_message)); 
	*position = *position + sizeof(gps_lat_message);

	return udp_packet;
}

void create_socket(char address, int port);