/**
 * @dir lib/Tasks
 * 
 * @brief Source and header files for RTOS tasks.
 * 
 * # Overview
 * 
 * These files contain the main tasks that are responsible for converting CAN messages
 * to UDP packets and UDP packets to CAN messages.
 * 
 * 
 * @file tasks.h
 * @author Clemens Jensen (clemens_jensen@yahoo.com)
 * @brief 
 * @version 0.1
 * @date 2022-05-17
 * 
 * @copyright Copyright (c) 2022
 * 
 */
#ifndef TASKS_H
#define TASKS_H

#include <stdio.h>
#include <string.h>
#include "lwip/err.h"
#include "lwip/sys.h"
#include "lwip/sockets.h"
#include <lwip/netdb.h>
#include "esp_netif.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/queue.h"
#include "freertos/event_groups.h"
#include "driver/gpio.h"
#include "driver/twai.h"
#include "esp_log.h"

#ifdef BUILD_FLAG_TRANS
#define RECEIVE_ADDR "192.168.4.1"
#define TRANSMIT_ADDR "192.168.4.2"
#define ENABLE_GPS 1
#endif

#ifdef BUILD_FLAG_REC
#define RECEIVE_ADDR "192.168.4.2"
#define TRANSMIT_ADDR "192.168.4.1"
#define ENABLE_GPS 0
#endif

/// UDP/IP Port (45500) that is used for communication between the modules.
#define PORT CONFIG_EXAMPLE_PORT

void init_can(void);

void task_CAN_to_UDP();

void task_UDP_to_CAN();

void *memcat(void *s1, size_t n1, void *s2, size_t n2);

void* add_message_to_packet(char* udp_packet, int* position, twai_message_t can_message);
#endif /* TASKS_H */