﻿using System;


using System.Threading;
using Peak.Can.Basic;
using TPCANHandle = System.UInt16;
using TPCANBitrateFD = System.String;
using TPCANTimestampFD = System.UInt64;


namespace CAN_Gui_Test
{ 
    class MyCAN
    {
        
        TPCANHandle channel = PCANBasic.PCAN_USBBUS1;
        TPCANBaudrate Baudrate = TPCANBaudrate.PCAN_BAUD_1M;
        TPCANMsg Buffer;
        TPCANTimestamp time;
        Tuple<TPCANMsg, TPCANTimestamp> msg;
        System.Collections.ArrayList LastMsgsList;
        private void Init_CAN_Interface()
        {
            TPCANStatus result = PCANBasic.Initialize(channel, Baudrate);

            if (result != TPCANStatus.PCAN_ERROR_OK)
            {
                Console.WriteLine(result.ToString());
            }
            else Console.WriteLine("\nInit erfolgreich )");
        }

        private void Uninit_CAN_Interface()
        {
            TPCANStatus result = PCANBasic.Uninitialize(channel);

            if (result != TPCANStatus.PCAN_ERROR_OK)
            {
                Console.WriteLine(result.ToString());
            }
            else Console.WriteLine("\nUninit erfolgreich");
        }
        private void Reset_Queue() 
        {
            TPCANStatus result = PCANBasic.Reset(channel);

            if (result != TPCANStatus.PCAN_ERROR_OK)
            {
                Console.WriteLine(result.ToString());
            }
            else Console.WriteLine("\nReset erfolgreich");
        }
        private void Get_Status()
        {
            TPCANStatus result = PCANBasic.GetStatus(channel);
            Console.WriteLine(result.ToString());
        }

        private void Read_From_Queue()
        { 
            TPCANStatus result;
 
            result = PCANBasic.Read(channel, out Buffer, out time);
            if (result != TPCANStatus.PCAN_ERROR_OK)
            {
                Console.WriteLine(result.ToString());
            }
            //LastMsgsList.Add
        }

    /*
        static void Main(string[] args)
        {
            var m = new MyCAN();
            m.channel = PCANBasic.PCAN_USBBUS1;
            m.Baudrate = TPCANBaudrate.PCAN_BAUD_1M;

            m.Init_CAN_Interface();

            while (true)
            {
                m.Read_From_Queue();
                if (m.Buffer.ID != 0) { 
                Console.WriteLine(m.Buffer.ID.ToString("X"));
                }
                Thread.Sleep(1);
            }
            
        }
    */
    }
}
