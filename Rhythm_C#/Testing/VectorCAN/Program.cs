﻿using System;
using vxlapi_NET;
using System.Threading;
using Microsoft.Win32.SafeHandles;

namespace VectorCAN
{
    class Program
    {
        public static XLDriver CANDemo = new XLDriver();
        public static String appName = "xlCANDemo";

        public static XLClass.xl_driver_config driverConfig = new XLClass.xl_driver_config();

        public static XLDefine.XL_HardwareType hwType = XLDefine.XL_HardwareType.XL_HWTYPE_NONE;
        public static uint hwIndex = 0;
        public static uint hwChannel = 0;
        public static int portHandle = -1;
        public static UInt64 accessMastk = 0;
        public static UInt64 permissionMask = 0;
        public static UInt64 txMask = 0;
        public static UInt64 rxMask = 0;
        public static int txCi = -1;
        public static int rxCi = -1;
        public static EventWaitHandle xlEvWaitHandle = new EventWaitHandle(false, EventResetMode.AutoReset, null);

        public static Thread rxThread;
        public static bool blockRxThread = false;


        [STAThread]
        static void Main(string[] args)
        {
            XLDefine.XL_Status status;

            Console.WriteLine("vxlapi_NET        : " + typeof(XLDriver).Assembly.GetName().Version);

            status = CANDemo.XL_OpenDriver();
            Console.WriteLine("Open Driver      : " + status);

            status = CANDemo.XL_GetDriverConfig(ref driverConfig);

            Console.WriteLine("Hello World!");

        }
    }
}
