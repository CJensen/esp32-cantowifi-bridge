﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;

namespace UDPAsync
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private async void button1_Click(object sender, RoutedEventArgs e)
        {
            await Task.Run(() =>
            {
                for (int i = 0; i < 1000; i++)
                {
                    Trace.WriteLine("Clicked: " + (i + 1));
                    Dispatcher.Invoke(() =>
                    {
                        text1.Text = "Clicked: " + (i + 1);
                    });
                    
                }
            });
            text1.Text = "Task is finished";
        }

    }
    
}
