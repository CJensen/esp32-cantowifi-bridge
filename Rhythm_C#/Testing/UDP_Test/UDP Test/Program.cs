﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;

public class UDPListener
{

    const int length = 8;
    public struct CANMessage
    {
        public int ID;
        public int DLC;
        public byte[] Data;
    }
    private const int listenPort = 45500;

    // TODO: Parallel threads
    private static void StartListener()
    {
        CANMessage parseMSG = new CANMessage();
        List<CANMessage> canMessages = new List<CANMessage>();

        UdpClient listener = new UdpClient(listenPort);
        IPEndPoint groupEP = new IPEndPoint(IPAddress.Any, listenPort);

        try
        {
            while (true)
            {
                Console.WriteLine("Waiting for Broadcast");
                byte[] test = listener.Receive(ref groupEP);
                Console.WriteLine($"Received broadcast from {groupEP} :");
                // Console.WriteLine($" {Encoding.ASCII.GetString(test, 0, test.Length)}");
                int MessageLength = 12;
                if (test.Length >= MessageLength && test.Length % MessageLength == 0)
                {
                    int NMessages = test.Length / MessageLength;
                    for (int j = 0; j < NMessages; j++)
                    {
                        int ii = j * MessageLength;
                        if (test[ii + 0] != 0xFF) break; // First Byte / Control Byte

                        parseMSG.ID = test[ii + 1] << 8 | test[ii + 2];   // 1-3
                        parseMSG.DLC = test[ii + 3];

                        int start = ii + 4;
                        parseMSG.Data = new byte[8];

                        // Populate Data field
                        for (int i = start; i < start + 8; i++)
                        {
                            parseMSG.Data[i - start] = test[i];
                        }


                        /*IF id vorhanden, nicht add sondern replace */
                        int index = canMessages.FindIndex(a => a.ID == parseMSG.ID);
                        if (index == -1)
                        {
                            canMessages.Add(parseMSG);
                        }
                        else
                        {
                            canMessages[index] = parseMSG;
                        }
                    }
                }
                Console.WriteLine($"Number of messages in List: {canMessages.Count()}");
            }
        }
        catch (SocketException e)
        {
            Console.WriteLine(e);

        }
        finally
        {
            listener.Close();
        }
    }

    public static void Main()
    {

        StartListener();
    }
}



