﻿/*
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;

namespace UDP_Framework
{

    public class UDPListener
    {
        const int lenght = 8;
        private const int listenPort = 45500;

        public struct CANMessage
        {
            public int ID;
            public int DLC;
            public byte[] Data;
        }

        public static void StartListener()
        {
            CANMessage parsedMsg = new CANMessage();
            List<CANMessage> canMessages = new List<CANMessage>();

            UdpClient listener = new UdpClient(listenPort);
            IPEndPoint groupEP = new IPEndPoint(IPAddress.Any, listenPort);

            try
            {
                while (true)
                {
                    Console.WriteLine("Waiting for Broadcast");
                    byte[] test = listener.Receive(ref groupEP);

                    int MessageLength = 12;

                    if (test.Length >= MessageLength && test.Length % MessageLength == 0)
                    {
                        int NMessages = test.Length / MessageLength;

                        for (int j = 0; j < NMessages; j++)
                        {
                            int ii = j * MessageLength;
                            if (test[ii + 0] != 0xFF) break;

                            parsedMsg.ID = test[ii + 1] << 8 | test[ii + 2];
                            parsedMsg.DLC = test[ii + 3];

                            int start = ii + 4;
                            parsedMsg.Data = new byte[8];
                            for (int i = start; i < start + 8; i++)
                            {
                                parsedMsg.Data[i - start] = test[i];
                            }

                            int index = canMessages.FindIndex(a => a.ID == parsedMsg.ID);
                            if (index == -1)
                            {
                                canMessages.Add(parsedMsg);
                            }
                            else
                            {
                                canMessages[index] = parsedMsg;
                            }
                        }
                    }
                    Console.WriteLine($"Numnber of messages in List: {canMessages.Count}");
                }
            }
            catch (Exception)
            {

                throw;
            }
        }



    }

    class Program : UDPListener
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            StartListener();
        }
    }

}
*/