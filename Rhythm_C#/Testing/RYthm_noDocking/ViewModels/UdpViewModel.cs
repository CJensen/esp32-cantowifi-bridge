﻿using RYthm_noDocking.Commands;
using RYthm_noDocking.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Net;
using System.Net.Sockets;
using System.Threading.Tasks;
using System.Windows.Data;
using System.Windows.Input;

namespace RYthm_noDocking.ViewModels
{
    class UdpViewModel : ObservableObject
    {
        private CANMessageModel _canModel;

        public UdpViewModel(int id, int dlc, byte[] data)
        {
            _canModel = new CANMessageModel(id, dlc, data);
        }

        public CANMessageModel Information
        {
            get
            {
                return _canModel;
            }
        }

    }
}
