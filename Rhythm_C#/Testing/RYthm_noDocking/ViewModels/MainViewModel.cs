﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RYthm_noDocking.ViewModels
{
    class MainViewModel
    {
        public ObservableCollection<UdpViewModel> Messages { get; private set; }


        public MainViewModel()
        {
            Messages = new ObservableCollection<UdpViewModel>()
            {
                new UdpViewModel(101, 8, new byte[8] {1,2,3,4,5,6,7,8 }),
                new UdpViewModel(102, 8, new byte[8] {9,8,7,6,5,4,3,2 }),
                new UdpViewModel(103, 8, new byte[8] {10,11,12,13,14,15,16,17 }),
                new UdpViewModel(104, 8, new byte[8] {1,2,3,4,5,6,7,8 })
            };
        }
    }
}
