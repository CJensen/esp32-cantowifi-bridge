﻿namespace RYthm_noDocking.Models
{
    public class CANMessageModel
    {
        #region Properties
        private int _id;
        public int ID
        {
            get { return _id; }
            set { _id = value; }
        }

        private int _dlc;
        public int DLC
        {
            get { return _dlc; }
            set { _dlc = value; }
        }

        private byte[] _data = new byte[8];

        public byte[] Data
        {
            get { return _data; }
            set { _data = value; }
        }

        #endregion

        public CANMessageModel(int Identifier, int DataLengthCounter, byte[] Data)
        {
            _id = Identifier;
            _dlc = DataLengthCounter;
            _data = Data;
        }
    }
}
