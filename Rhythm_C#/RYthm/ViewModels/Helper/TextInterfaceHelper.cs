﻿using RYthm.Models;
using System;
using vxlapi_NET;

namespace RYthm.ViewModels.Helper
{
    public class TextInterfaceHelper
    {
        public static string TextInterfaceBuffer;
        public static char TextInterfaceBoardId;
        public static int TextInterfaceCountRemaining;
        public static int CurrentParameterId;
        public static Parameters CurrentParameter = new Parameters();

        public static string HandleTextInterfaceMessages(XLClass.xl_event canMessage)
        {
            if (canMessage.tagData.can_Msg.id >= 0x7a0 & canMessage.tagData.can_Msg.id <= 0x7b1)
            {
                if (canMessage.tagData.can_Msg.id == 0x7a0)
                {
                    TextInterfaceBuffer = "";
                    TextInterfaceCountRemaining = canMessage.tagData.can_Msg.data[1];
                    TextInterfaceBoardId = Convert.ToChar(canMessage.tagData.can_Msg.data[0]);
                }

                for (int i = 2; i < 8; i++)
                {
                    char Char = Convert.ToChar(canMessage.tagData.can_Msg.data[i]);
                    if (Char != '\u0000')
                        TextInterfaceBuffer += Char;
                }
                TextInterfaceCountRemaining -= 1;

                if (TextInterfaceCountRemaining == 0)
                {
                    TextInterfaceBuffer = TextInterfaceBoardId + TextInterfaceBuffer;
                    // MessageBox.Show(TextInterfaceBuffer);
                    return TextInterfaceBuffer;
                }

                if (canMessage.tagData.can_Msg.id == 0x7b1)
                {
                    CurrentParameter.Minimum = canMessage.tagData.can_Msg.data[3] << 8 | canMessage.tagData.can_Msg.data[4];
                    if (CurrentParameter.Type == "int16" & CurrentParameter.Minimum > 32767)
                    {
                        CurrentParameter.Minimum = CurrentParameter.Minimum - 65536;
                    }
                    CurrentParameter.Maximum = canMessage.tagData.can_Msg.data[5] << 8 | canMessage.tagData.can_Msg.data[6];
                    CurrentParameter.HasMaxMin = true;
                }
            }

            return "0";
        }

        public static object ParseTextInterfaceString(string buffer)
        {
            if (buffer == "0")
            {
                return -1;
            }
            Board currentBoard = new Board();

            if (buffer[1] == 224) //Board Message
            {
                currentBoard.ID = Convert.ToUInt16(buffer[0]);
                currentBoard.Name = buffer.Substring(2, 3); //TODO: no hardcoded places
                CurrentParameter.ID = 0;
                CurrentParameterId = 0;
                return currentBoard;
            }
            else if (Equals(buffer[1], '\u00f0')) // message is parameter
            {
                string[] splitStrings = buffer.Split(':');
                int end = splitStrings[0].Length;
                int start = 2;
                CurrentParameter.Type = splitStrings[0].Substring(2, end - start);
                int.TryParse(splitStrings[1], out int valueResult);
                CurrentParameter.Value = valueResult;
                return CurrentParameter;
            }
            else if (Equals(buffer[1], '\u00f1')) // message is watch
            {

            }
            else if (Equals(buffer[1], '\u0026')) // message is "&clear"
            {
                //int i = 1;
            }
            else if (Equals(buffer[1], '\u00f4')) // message is Version: x
            {
                //int i = 1;
            }
            else //message is parameter name
            {
                CurrentParameter = new Parameters();
                int start = 3;
                int end = buffer.Length;
                CurrentParameter.ParameterName = buffer.Substring(start, end - start);
                CurrentParameter.ID = Convert.ToChar(buffer.Substring(2, 1)) - 50;
                CurrentParameterId += 1;
            }

            return null;

        }
    }
}
