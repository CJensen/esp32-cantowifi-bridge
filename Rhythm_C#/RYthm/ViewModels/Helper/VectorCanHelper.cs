﻿using Microsoft.Win32.SafeHandles;
using RYthm.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using vxlapi_NET;

namespace RYthm.ViewModels.Helper
{
    public class VectorCanHelper
    {
        private static XLDriver CANDriver = new XLDriver();
        private static String appName = "RYthm";

        // Driver configuration
        private static XLClass.xl_driver_config driverConfig = new XLClass.xl_driver_config();

        // Variables required by XLDriver
        private static XLDefine.XL_HardwareType hwType = XLDefine.XL_HardwareType.XL_HWTYPE_NONE;
        private static uint hwIndex = 0;
        private static uint hwChannel = 0;
        private static int portHandle = -1;
        private static UInt64 accessMask = 0;
        private static UInt64 permissionMask = 0;
        private static UInt64 txMask = 0;
        private static int txCi = -1;
        private static EventWaitHandle xlEvWaitHandle = new EventWaitHandle(false, EventResetMode.AutoReset, null);
        // RX thread
        private static Thread rxThread;
        private static bool blockRxThread = false;

        // RX event
        public event EventHandler<XLClass.xl_event> RxMessageReceivedEvent;

        /// <summary>
        /// Initializes Vector CAN Transceiver and starts receive thread (currently VN 1610)
        /// </summary>
        public void VectorCan_Init()
        {
            XLDefine.XL_Status status;

            // Open XL Driver
            status = CANDriver.XL_OpenDriver();
            if (status != XLDefine.XL_Status.XL_SUCCESS) PrintFunctionError();

            // Get XL Driver configuration
            status = CANDriver.XL_GetDriverConfig(ref driverConfig);
            if (status != XLDefine.XL_Status.XL_SUCCESS) PrintFunctionError();

            // If the application name cannot be found in VCANCONF...
            if ((CANDriver.XL_GetApplConfig(appName, 0, ref hwType, ref hwIndex, ref hwChannel, XLDefine.XL_BusTypes.XL_BUS_TYPE_CAN) != XLDefine.XL_Status.XL_SUCCESS))
            {
                //...create the item with two CAN channels
                CANDriver.XL_SetApplConfig(appName, 0, XLDefine.XL_HardwareType.XL_HWTYPE_NONE, 0, 0, XLDefine.XL_BusTypes.XL_BUS_TYPE_CAN);
                PrintAssignErrorAndPopupHwConf();
            }
            
            // Request the user to assign channels until both CAN1 (Tx) and CAN2 (Rx) are assigned to usable channels
            while (!GetAppChannelAndTestIsOk(0, ref txMask, ref txCi))
            {
                PrintAssignErrorAndPopupHwConf();
            }

            accessMask = txMask;
            permissionMask = accessMask;

            // Open port
            status = CANDriver.XL_OpenPort(ref portHandle, appName, accessMask, ref permissionMask, 1024, XLDefine.XL_InterfaceVersion.XL_INTERFACE_VERSION, XLDefine.XL_BusTypes.XL_BUS_TYPE_CAN);
            if (status != XLDefine.XL_Status.XL_SUCCESS) PrintFunctionError();

            // Check port
            status = CANDriver.XL_CanRequestChipState(portHandle, accessMask);
            if (status != XLDefine.XL_Status.XL_SUCCESS) PrintFunctionError();

            // Activate channel
            status = CANDriver.XL_ActivateChannel(portHandle, accessMask, XLDefine.XL_BusTypes.XL_BUS_TYPE_CAN, XLDefine.XL_AC_Flags.XL_ACTIVATE_NONE);
            if (status != XLDefine.XL_Status.XL_SUCCESS) PrintFunctionError();

            // Initialize EventWaitHandle object with RX event handle provided by DLL
            int tempInt = -1;
            status = CANDriver.XL_SetNotification(portHandle, ref tempInt, 1);
            xlEvWaitHandle.SafeWaitHandle = new SafeWaitHandle(new IntPtr(tempInt), true);
            if (status != XLDefine.XL_Status.XL_SUCCESS) PrintFunctionError();

            // Reset time stamp clock
            status = CANDriver.XL_ResetClock(portHandle);
            if (status != XLDefine.XL_Status.XL_SUCCESS) PrintFunctionError();

            // Run Rx Thread
            rxThread = new Thread(new ThreadStart(RXThread));
            rxThread.Name = "RxThread";
            rxThread.Start();
        }
        /// <summary>
        /// Receive Thread to receive can messages
        /// </summary>
        public void RXThread()
        {
            // Create new object containing received data 
            XLClass.xl_event receivedEvent = new XLClass.xl_event();

            // Result of XL Driver function calls
            XLDefine.XL_Status xlStatus = XLDefine.XL_Status.XL_SUCCESS;

            // Note: this thread will be destroyed by MAIN
            while (true)
            {
                // Wait for hardware events
                if (xlEvWaitHandle.WaitOne(1000))
                {
                    // ...init xlStatus first
                    xlStatus = XLDefine.XL_Status.XL_SUCCESS;

                    // afterwards: while hw queue is not empty...
                    while (xlStatus != XLDefine.XL_Status.XL_ERR_QUEUE_IS_EMPTY)
                    {
                        // ...block RX thread to generate RX-Queue overflows
                        while (blockRxThread) { Thread.Sleep(1000); }

                        // ...receive data from hardware.
                        xlStatus = CANDriver.XL_Receive(portHandle, ref receivedEvent);

                        //  If receiving succeed....
                        if (xlStatus == XLDefine.XL_Status.XL_SUCCESS)
                        {
                            if ((receivedEvent.flags & XLDefine.XL_MessageFlags.XL_EVENT_FLAG_OVERRUN) != 0)
                            {
                                Console.WriteLine("-- XL_EVENT_FLAG_OVERRUN --");
                            }

                            // ...and data is a Rx msg...
                            if (receivedEvent.tag == XLDefine.XL_EventTags.XL_RECEIVE_MSG)
                            {
                                if ((receivedEvent.tagData.can_Msg.flags & XLDefine.XL_MessageFlags.XL_CAN_MSG_FLAG_OVERRUN) != 0)
                                {
                                    Console.WriteLine("-- XL_CAN_MSG_FLAG_OVERRUN --");
                                }

                                // ...check various flags
                                if ((receivedEvent.tagData.can_Msg.flags & XLDefine.XL_MessageFlags.XL_CAN_MSG_FLAG_ERROR_FRAME)
                                    == XLDefine.XL_MessageFlags.XL_CAN_MSG_FLAG_ERROR_FRAME)
                                {
                                    Console.WriteLine("ERROR FRAME");
                                }

                                else if ((receivedEvent.tagData.can_Msg.flags & XLDefine.XL_MessageFlags.XL_CAN_MSG_FLAG_REMOTE_FRAME)
                                    == XLDefine.XL_MessageFlags.XL_CAN_MSG_FLAG_REMOTE_FRAME)
                                {
                                    Console.WriteLine("REMOTE FRAME");
                                }

                                else
                                {
                                    string eventbase = CANDriver.XL_GetEventString(receivedEvent);
                                    RxMessageReceivedEvent?.Invoke(this, receivedEvent);
                                }
                                
                            }
                        }
                    }
                }
                // No event occurred
            }
        }

        public void StopThread()
        {
            rxThread.Abort();
        }

        /// <summary>
        /// Send message to CAN Bus
        /// </summary>
        public void VectorCan_Transmit(CanMessageModel canMessage)
        {
            XLDefine.XL_Status txStatus;

            // Create an event collection with 2 messages (events)
            XLClass.xl_event_collection xlEventCollection = new XLClass.xl_event_collection(1);

            // event
            xlEventCollection.xlEvent[0].tagData.can_Msg.id = canMessage.Identifier;
            xlEventCollection.xlEvent[0].tagData.can_Msg.dlc = (ushort) canMessage.DataLenghtCode;
            xlEventCollection.xlEvent[0].tagData.can_Msg.data[0] = canMessage.Data[0];
            xlEventCollection.xlEvent[0].tagData.can_Msg.data[1] = canMessage.Data[1];
            xlEventCollection.xlEvent[0].tagData.can_Msg.data[2] = canMessage.Data[2];
            xlEventCollection.xlEvent[0].tagData.can_Msg.data[3] = canMessage.Data[3];
            xlEventCollection.xlEvent[0].tagData.can_Msg.data[4] = canMessage.Data[4];
            xlEventCollection.xlEvent[0].tagData.can_Msg.data[5] = canMessage.Data[5];
            xlEventCollection.xlEvent[0].tagData.can_Msg.data[6] = canMessage.Data[6];
            xlEventCollection.xlEvent[0].tagData.can_Msg.data[7] = canMessage.Data[7];
            xlEventCollection.xlEvent[0].tag = XLDefine.XL_EventTags.XL_TRANSMIT_MSG;

            // Transmit events
            txStatus = CANDriver.XL_CanTransmit(portHandle, txMask, xlEventCollection);
        }

        public void OpenVectorHardwareConfigurator()
        {
            PrintAssignErrorAndPopupHwConf();

        }

        public void CloseDriver()
        {
            CANDriver.XL_DeactivateChannel(portHandle, accessMask);
            CANDriver.XL_ClosePort(portHandle);
        }

        private static int PrintFunctionError()
        {
            _ = MessageBox.Show("ERROR: Function call failed!\nPress any key to continue...");
            return -1;
        }
        private static void PrintAssignErrorAndPopupHwConf()
        {
            CANDriver.XL_PopupHwConfig();
            MessageBox.Show("Please check application settings of \"" + appName + " CAN1\",\nassign them to available hardware channels and press enter.");
        }

        private static bool GetAppChannelAndTestIsOk(uint appChIdx, ref UInt64 chMask, ref int chIdx)
        {
            XLDefine.XL_Status status = CANDriver.XL_GetApplConfig(appName, appChIdx, ref hwType, ref hwIndex, ref hwChannel, XLDefine.XL_BusTypes.XL_BUS_TYPE_CAN);
            if (status != XLDefine.XL_Status.XL_SUCCESS)
            {
                Console.WriteLine("XL_GetApplConfig      : " + status);
                PrintFunctionError();
            }

            chMask = CANDriver.XL_GetChannelMask(hwType, (int)hwIndex, (int)hwChannel);
            chIdx = CANDriver.XL_GetChannelIndex(hwType, (int)hwIndex, (int)hwChannel);
            if (chIdx < 0 || chIdx >= driverConfig.channelCount)
            {
                // the (hwType, hwIndex, hwChannel) triplet stored in the application configuration does not refer to any available channel.
                return false;
            }

            // test if CAN is available on this channel
            return (driverConfig.channel[chIdx].channelBusCapabilities & XLDefine.XL_BusCapabilities.XL_BUS_ACTIVE_CAP_CAN) != 0;
        }
    }
}