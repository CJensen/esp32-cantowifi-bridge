﻿using System;
using System.IO;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Threading;
using Newtonsoft.Json;
using RYthm.Core;
using RYthm.Models;
using RYthm.ViewModels.Helper;
using vxlapi_NET;
using Microsoft.Win32;

namespace RYthm.ViewModels
{
    public class TextInterfaceViewModel : ObservableObject
    {
        private ObservableCollection<Parameters> _parametersList;
        public ObservableCollection<Parameters> ParametersList
        {
            get => _parametersList;
            set
            {
                _parametersList = value;
                OnPropertyChanged(nameof(ParametersList));
            }
        }

        private ObservableCollection<LaptimeInfo> laptimeInfoList;

        public ObservableCollection<LaptimeInfo> LaptimeInfoList
        {
            get { return laptimeInfoList; }
            set 
            { 
                laptimeInfoList = value;
                OnPropertyChanged(nameof(LaptimeInfoList));
            }
        }


        public ObservableCollection<Board> BoardList { get; set; }

        private Board _selectedBoard;
        public Board SelectedBoard
        {
            get => _selectedBoard;
            set
            {
                _selectedBoard = value;
                OnPropertyChanged(nameof(SelectedBoard));
                // if (value != null) ParametersList = value.ParameterList;
            }
        }

        private Parameters _selectedParameters;
        public Parameters SelectedParameters
        {
            get => _selectedParameters;
            set
            {
                _selectedParameters = value;
                OnPropertyChanged(nameof(SelectedParameters));
            }
        }

        private Brush _backgroundColor;
        public Brush BackgroundColor
        {
            get { return _backgroundColor; }
            set
            {
                _backgroundColor = value;
                OnPropertyChanged(nameof(BackgroundColor));
            }
        }

        private int _lastValue;

        public int LastValue
        {
            get { return _lastValue; }
            set
            {
                _lastValue = value;
                OnPropertyChanged(nameof(LastValue));
            }
        }

        public Board CurrentBoard = new Board();
        public Parameters CurrentParameters = new Parameters();

        #region Commands

        public ICommand ScanForBoardCommand { get; set; }
        public ICommand SetCommand { get; set; }
        public ICommand StoreCommand { get; set; }
        public ICommand ScanForParametersCommand { get; set; }
        public ICommand TestCommand { get; set; }
        public ICommand IsEditedCommand { get; set; }
        public ICommand CanSettingsCommand { get; set; }
        public ICommand SaveToFileCommand { get; set; }
        public ICommand LoadFromFileCommand { get; set; }
        public ICommand CloseProgramCommand { get; set;}
        #endregion

        private readonly VectorCanHelper _vectorCan = new VectorCanHelper();

        public TextInterfaceViewModel()
        {
            BoardList = new ObservableCollection<Board>();
            ParametersList = new ObservableCollection<Parameters>();
            LaptimeInfoList = new ObservableCollection<LaptimeInfo>();


            SelectedBoard = new Board();
            SelectedParameters = new Parameters();

            ScanForBoardCommand = new RelayCommand(GetBoards);
            SetCommand = new RelayCommand(SetParameter);
            StoreCommand = new RelayCommand(StoreParameter);
            ScanForParametersCommand = new RelayCommand(GetParameters);
            IsEditedCommand = new RelayCommand(ChangeRowColor);
            CanSettingsCommand = new RelayCommand(OpenVectorSettings);
            SaveToFileCommand = new RelayCommand(SaveParameters);
            LoadFromFileCommand = new RelayCommand(LoadParameters);

            CloseProgramCommand = new RelayCommand(CloseProgram);

            TestCommand = new RelayCommand(TestMethod);

            _vectorCan.VectorCan_Init();
            _vectorCan.RxMessageReceivedEvent += VectorCan_RxMessageReceivedEvent;


            #region TestCode

            if (DesignerProperties.GetIsInDesignMode(new System.Windows.DependencyObject()))
            {
            BackgroundColor = Brushes.Yellow;
            var board = new Board();
                board.ID = 3;
                board.Name = "Test";
                BoardList.Add(board);
            var param = new Parameters();
                param.ID = 1;
                param.ParameterName = "Test1";
                param.RowColor = "f0f0f0";
                ParametersList.Add(param);
            var param2 = new Parameters();
                param2.ID = 2;
                param2.ParameterName = "testing_parameter";
                ParametersList.Add(param2);
            var lap1 = new LaptimeInfo();
                lap1.LapNumber =1;
                lap1.Laptime = 4.4212;
                LaptimeInfoList.Add(lap1);
            BoardList[0].ParameterList.Add(param);
            }

            #endregion
        }

        private void CloseProgram(object obj)
        {
            _vectorCan.StopThread();
            Application.Current.Shutdown();
        }

        private void LoadParameters(object obj)
        {
            // Configure open file dialog box
            Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog();
            dlg.FileName = "TI_parameters"; // Default file name
            dlg.DefaultExt = ".json"; // Default file extension
            dlg.Filter = "json (.json)|*.json"; // Filter files by extension

            // Show open file dialog box
            Nullable<bool> result = dlg.ShowDialog();

            // Process open file dialog box results
            if (result == true)
            {
                // Open document
                string filename = dlg.FileName;
                string readText = File.ReadAllText(filename);
                ObservableCollection<Parameters> c_obj = JsonConvert.DeserializeObject<ObservableCollection<Parameters>>(readText);
                ParametersList = c_obj;
            }
        }

        private void SaveParameters(object obj)
        {

            // Configure save file dialog box
            var dialog = new Microsoft.Win32.SaveFileDialog();
            dialog.FileName = "TI_parameters"; // Default file name
            dialog.DefaultExt = ".json"; // Default file extension
            dialog.Filter = "JSON|*.json"; // Filter files by extension

            // Show save file dialog box
            bool? result = dialog.ShowDialog();

            // Process save file dialog box results
            if (result == true)
            {
                // Save document
                string filename = dialog.FileName;
                string jsonString = JsonConvert.SerializeObject(ParametersList);
                File.WriteAllText(filename, jsonString);
                
            }
            //MessageBox.Show("Not Implemented yet");
        }
        private void OpenVectorSettings(object obj)
        {
            _vectorCan.RxMessageReceivedEvent -= VectorCan_RxMessageReceivedEvent;
            _vectorCan.CloseDriver();
            _vectorCan.OpenVectorHardwareConfigurator();
            //Close vector and re-init

            _vectorCan.VectorCan_Init();
            _vectorCan.RxMessageReceivedEvent += VectorCan_RxMessageReceivedEvent;

        }
        private void ChangeRowColor(object obj)
        {
            SelectedParameters.RowColor = "#cb9648";
        }
        public void GetBoards(object obj)
        {
            BoardList.Clear();
            ParametersList.Clear();
            var transmitMessage = new CanMessageModel();
            for (uint i = 0x771; i <= 0x77f; i++)
            {
                Thread.Sleep(10);
                transmitMessage.Identifier = i;
                transmitMessage.DataLenghtCode = 1;
                transmitMessage.Data[0] = 0x00;
                _vectorCan.VectorCan_Transmit(transmitMessage);
            }

            //MessageBox.Show(obj.ToString());
        }
        public void GetParameters(object obj)
        {
            ParametersList.Clear();
            var transmitMessage = new CanMessageModel();
            if (obj is Board)
            {
                //int id = obj.ID;
                transmitMessage.Identifier = 0x770 + SelectedBoard.ID;
                transmitMessage.DataLenghtCode = 1;
                transmitMessage.Data[0] = Convert.ToByte('h');
                _vectorCan.VectorCan_Transmit(transmitMessage);
            }
            else
            {
                MessageBox.Show("Select a Board!");
            }

            //int boardID = obj.ID;


            //TODO: Implement get routine
        }
        private void SetParameter(object obj)
        {
            var transmitMessage = new CanMessageModel();
            if (obj is Parameters)
            {
                transmitMessage.Identifier = 0x770 + SelectedBoard.ID;

                transmitMessage.Data[0] = Convert.ToByte('u');

                transmitMessage.Data[1] = Convert.ToByte(SelectedParameters.ID);
                if (SelectedParameters.Type == "uint8")
                {
                    transmitMessage.DataLenghtCode = 3;
                    transmitMessage.Data[2] = Convert.ToByte(SelectedParameters.Value);  //TODO: catch wrong input
                }
                else
                {
                    transmitMessage.DataLenghtCode = 4;
                    transmitMessage.Data[2] = Convert.ToByte(SelectedParameters.Value & 0xff);
                    transmitMessage.Data[3] = Convert.ToByte(SelectedParameters.Value >> 8 & 0xff);
                }

                _vectorCan.VectorCan_Transmit(transmitMessage);
                SelectedParameters.RowColor = "#6e0000";
                LastValue = SelectedParameters.Value;
            }
            else
            {
                MessageBox.Show("Select a Parameter!");
            }
        }
        private void StoreParameter(object obj)
        {
            var transmitMessage = new CanMessageModel();
            if (obj is Parameters)
            {
                transmitMessage.Identifier = 0x770 + SelectedBoard.ID;

                transmitMessage.Data[0] = Convert.ToByte('s');

                transmitMessage.Data[1] = Convert.ToByte(SelectedParameters.ID);
                if (SelectedParameters.Type == "uint8")
                {
                    transmitMessage.DataLenghtCode = 3;
                    transmitMessage.Data[2] = Convert.ToByte(SelectedParameters.Value);
                }
                else
                {
                    transmitMessage.DataLenghtCode = 4;
                    transmitMessage.Data[2] = Convert.ToByte(SelectedParameters.Value & 0xff);
                    transmitMessage.Data[3] = Convert.ToByte(SelectedParameters.Value >> 8 & 0xff);
                }

                _vectorCan.VectorCan_Transmit(transmitMessage);
                SelectedParameters.RowColor = "#6e0000";
                LastValue = SelectedParameters.Value;
            }
            else
            {
                MessageBox.Show("Select a Board!");
            }
        }
        private void VectorCan_RxMessageReceivedEvent(object sender, XLClass.xl_event e)
        {
            // Returns String from can message for further steps
            var test = TextInterfaceHelper.HandleTextInterfaceMessages(e);

            var returnValue = TextInterfaceHelper.ParseTextInterfaceString(test);
            if (returnValue is Board)
            {
                CurrentBoard = returnValue as Board;
                var alreadyExists = BoardList.Any(board => board.ID == CurrentBoard.ID);

                if (!alreadyExists)
                {
                    Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Send,
                        new Action(() => BoardList.Add(CurrentBoard)));
                }
            }

            if (returnValue is Parameters)
            {
                CurrentParameters = returnValue as Parameters;
                var alreadyExists = ParametersList.Any(parameter => parameter.ID == CurrentParameters.ID);
                if (!alreadyExists)
                {
                    Application.Current.Dispatcher.Invoke(DispatcherPriority.Send,
                        new Action(() => ParametersList.Add(CurrentParameters)));
                }
                else
                {
                    if (CurrentParameters.Value == LastValue)
                    {
                        CurrentParameters.RowColor = "#006000";
                    }
                    Application.Current.Dispatcher.Invoke(DispatcherPriority.Send,
                        new Action(() => ParametersList[CurrentParameters.ID] = CurrentParameters));


                }

                // _uiContext.Send(x => BoardList[CurrentBoard.ID - 1].ParameterList.Add(CurrentParameters), null);
            }

            returnValue = null;
        }
        private void TestMethod(object obj)
        {
            
            string parameter = obj.ToString();

            //SelectedParameters.RowColor = "Yellow";

            if (parameter == "1")
            {
                BackgroundColor = Brushes.Green;
            }

            if (parameter == "0")
            {
                BackgroundColor = Brushes.Yellow;
            }
        }
    }
}