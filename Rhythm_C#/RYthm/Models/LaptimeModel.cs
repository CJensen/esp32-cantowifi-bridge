﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RYthm.Core;

namespace RYthm.Models
{
    public class LaptimeInfo : ObservableObject
    {
        private int lapNumber;
        private double laptime;

        public double Laptime
        {
            get { return laptime; }
            set 
            { 
                laptime = value; 
                OnPropertyChanged(nameof(Laptime));
            }
        }

        public int LapNumber
        {
            get { return lapNumber; }
            set 
            { 
                lapNumber = value;
                OnPropertyChanged(nameof(LapNumber));
            }
        }

    }
}
