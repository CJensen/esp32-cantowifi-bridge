﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RYthm.Core;

namespace RYthm.Models
{
    public class Board
    {
        public uint ID { get; set; }
        public string Name { get; set; }
        public ObservableCollection<Parameters> ParameterList { get; set; }

        public Board()
        {
            ParameterList = new ObservableCollection<Parameters>();
        }
    }

    public class Parameters : ObservableObject
    {
        private int _id;
        private string _parameterName;
        private int _value;
        private int _maximum;
        private int _minimum;
        private string _type;
        private string _rowColor;
        private bool _hasMinMaxValuue;

        public bool HasMaxMin
        {
            get { return _hasMinMaxValuue; }
            set { _hasMinMaxValuue = value; }
        }


        public string RowColor
        {
            get { return _rowColor; }
            set
            {
                _rowColor = value;
                OnPropertyChanged(nameof(RowColor));
            }
        }

        public int ID
        {
            get => _id;
            set
            {
                _id = value;
                OnPropertyChanged(nameof(ID));
            }
        }

        public string ParameterName
        {
            get => _parameterName;
            set
            {
                _parameterName = value;
                OnPropertyChanged(nameof(ParameterName));
            }
        }

        public int Value
        {
            get => _value;
            set
            {
                if (HasMaxMin)
                {
                    value = CheckBoundaries(value);
                }
                _value = value;
                OnPropertyChanged(nameof(Value));
            }
        }

        private int CheckBoundaries(int value)
        {
            if( value < Minimum )
            {
                value = Minimum;
                return value;
            }
            else if( value > Maximum )
            {
                value = Maximum;
                return value;
            }
            else
            {
                return value;
            }
        }

        public int Maximum
        {
            get => _maximum;
            set
            {
                _maximum = value;
                OnPropertyChanged(nameof(Maximum));
            }
        }

        public int Minimum
        {
            get => _minimum;
            set
            {
                _minimum = value;
                OnPropertyChanged(nameof(Minimum));
            }
        }

        public string Type
        {
            get => _type;
            set
            {
                _type = value;
                OnPropertyChanged(nameof(Type));
            }
        }
    }
}
