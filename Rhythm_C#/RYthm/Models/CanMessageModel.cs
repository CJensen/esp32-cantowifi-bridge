﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RYthm.Models
{
    public class CanMessageModel
    {
        public uint Identifier { get; set; }
        public int DataLenghtCode { get; set; }
        public byte[] Data { get; set; }

        public CanMessageModel()
        {
            Data = new byte[8];
        }
    }
}
